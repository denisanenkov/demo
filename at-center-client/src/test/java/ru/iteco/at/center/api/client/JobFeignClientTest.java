package ru.iteco.at.center.api.client;

import org.junit.jupiter.api.Test;
import ru.iteco.at.center.enumerate.SystemCapacity;
import ru.iteco.at.center.model.dto.JobDTO;
import ru.iteco.at.center.model.dto.JobInstanceDTO;
import ru.iteco.at.center.model.dto.SystemDTO;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertNotNull;

class JobFeignClientTest {

    private static final Integer PORT = 8080;

    @Test
    void post() {
        final JobInstanceDTO jobInstanceDTO = new JobInstanceDTO();
        final JobDTO jobDTO = new JobDTO();
        SystemDTO systemDTO = new SystemDTO();
        systemDTO.setDescription("testSystem");
        systemDTO.setSystemCapacity(SystemCapacity.CAPACITY_64);
        systemDTO.setName("Windows 10");
        jobInstanceDTO.setSystemDTO(systemDTO);
        jobInstanceDTO.setJobId(jobDTO.getId());
        jobInstanceDTO.setDateBegin(new Date());
        jobInstanceDTO.setDateEnd(new Date());
        jobDTO.setUserToken("$2a$10$.z8RxKjzYgn0Twlg8yphTeFe4pLozgCNSMCm4GubuoekbAOAa2OOK");
        jobDTO.setName("job from client");
        jobDTO.setBuildNumber("1.0.0");
        jobDTO.setJobInstanceDTO(jobInstanceDTO);

        final JobFeignClient postClient = JobFeignClient.client("http://localhost:" + PORT + "/api/job");
        assertNotNull(postClient);
        postClient.post(jobDTO);
        final JobFeignClient deleteClient = JobFeignClient.client("http://localhost:" + PORT + "/api/job/deleteJob");
        deleteClient.deleteJob(jobDTO);
    }
}