package ru.iteco.at.center.api.client;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import ru.iteco.at.center.client.UserFeignClient;
import ru.iteco.at.center.model.dto.UserDTO;

import java.util.*;

public class UserFeignClientTest {

    @NotNull
    private final UserFeignClient userClient = new UserFeignClient();

    private String firstId;

    private String secondId;

    private String thirdId;

    private UserDTO firstUser;

    private UserDTO secondUser;

    private UserDTO thirdUser;

    public void init() {

        firstId = UUID.randomUUID().toString();
        secondId = UUID.randomUUID().toString();
        thirdId = UUID.randomUUID().toString();

        firstUser = new UserDTO(
                "testingUser1", "testingUser", false,
                "$2a$10$.z8RxKjzYgnvr7arye8gw4gnqvpaebr7e8baqerg4ioesAOAa2OOK",
                new HashSet<>(Arrays.asList("USER", "ADMIN")), "firsttest@mail.ru",
                "Petrov", "Petr", "Petrovich");
        firstUser.setId(firstId);

        secondUser = new UserDTO(
                "testingUser2", "testingUser", false,
                "$2a$10$.z8RxKjzYgn0Twlg8yphTeFe4pLozgCNSMCm4GubuoekbAOAa2wOS",
                new HashSet<>(Arrays.asList("USER", "ADMIN")), "testsecond@mail.ru",
                "Georgiev", "Georgy", "Georgievich");
        secondUser.setId(secondId);

        thirdUser = new UserDTO(
                "testingUser3", "testingUser", false,
                "$2a$10$.zIHOvsbiod987y6hipufWEjinsv34hg934vwvwmIWIVO8ewwfOrT",
                new HashSet<>(Arrays.asList("USER", "ADMIN")), "testthird@mail.ru",
                "Ivanov", "Ivanov", "Ivanovich");
        thirdUser.setId(thirdId);

    }

    public void destroy() {
        userClient.deleteUserById(firstUser.getId());
        userClient.deleteUserById(secondUser.getId());
        userClient.deleteUserById(thirdUser.getId());
    }

    @Test
    void postAndUpdateUserIntegrationTest() {
        try {
            init();
            long countBeforeSave = userClient.countUsers();
            userClient.saveUser(firstUser);
            long countAfterSave = userClient.countUsers();
            Assertions.assertEquals(countBeforeSave + 1, countAfterSave);
            assert firstUser.getLogin() != null;
            Assertions.assertTrue(userClient.existsByLogin(firstUser.getLogin()));
            String login = UUID.randomUUID().toString();
            firstUser.setLogin(login);
            userClient.saveUser(firstUser);
            long countAfterUpdate = userClient.countUsers();
            Assertions.assertTrue(userClient.existsByLogin(login));
            Assertions.assertEquals(countAfterSave, countAfterUpdate);
        } finally {
            destroy();
        }
    }

    @Test
    void findAllUsersIntegrationTest() {
        try {
            init();
            long beforeSave = userClient.countUsers();
            List<UserDTO> userDTOListBeforeSave = userClient.findAllUsers();
            userClient.saveUser(firstUser);
            userClient.saveUser(secondUser);
            userClient.saveUser(thirdUser);
            List<UserDTO> userDTOListAfterSave = userClient.findAllUsers();
            long afterSave = userClient.countUsers();
            Assertions.assertEquals(userDTOListBeforeSave.size() + 3, userDTOListAfterSave.size());
            Assertions.assertEquals(beforeSave + 3, afterSave);
        } finally {
            destroy();
        }
    }

    @Test
    void findAllUsersByParameterIntegrationTest() {
        try {
            init();
            @Nullable final String marker = UUID.randomUUID().toString();
            @Nullable Set<UserDTO> nullList = userClient.findUsersByParameter(marker);
            long nullCount = nullList.size();
            firstUser.setToken(marker);
            userClient.saveUser(firstUser);
            userClient.saveUser(secondUser);
            userClient.saveUser(thirdUser);
            @Nullable final Set<UserDTO> firstList = userClient.findUsersByParameter(marker);
            Assertions.assertEquals(nullList.size() + 1, firstList.size());
            secondUser.setFirstName(marker);
            userClient.saveUser(secondUser);
            @Nullable final Set<UserDTO> secondList = userClient.findUsersByParameter(marker);
            Assertions.assertEquals(nullList.size() + 2, secondList.size());
            thirdUser.setEmail(marker);
            userClient.saveUser(thirdUser);
            @Nullable final Set<UserDTO> thirdList = userClient.findUsersByParameter(marker);
            Assertions.assertEquals(nullList.size() + 3, thirdList.size());
            Assertions.assertEquals(secondList.size() + 1, thirdList.size());
            Assertions.assertEquals(firstList.size() + 2, thirdList.size());
        } finally {
            destroy();
        }
    }

    @Test
    void existsByLoginIntegrationTest() {
        try {
            init();
            @Nullable final String login = UUID.randomUUID().toString();
            Assertions.assertFalse(userClient.existsByLogin(login));
            firstUser.setLogin(login);
            userClient.saveUser(firstUser);
            Assertions.assertTrue(userClient.existsByLogin(login));
            userClient.deleteUserById(firstUser.getId());
            assert firstUser.getLogin() != null;
            Assertions.assertFalse(userClient.existsByLogin(firstUser.getLogin()));
        } finally {
            destroy();
        }
    }

    @Test
    void countUsersIntegrationTest() {
        try {
            init();
            long beforeSave = userClient.countUsers();
            userClient.saveUser(firstUser);
            long afterFirstSave = userClient.countUsers();
            Assertions.assertEquals(beforeSave + 1, afterFirstSave);
            userClient.saveUser(secondUser);
            long afterSecondSave = userClient.countUsers();
            Assertions.assertEquals(beforeSave + 2, afterSecondSave);
            Assertions.assertEquals(afterFirstSave + 1, afterSecondSave);
            userClient.saveUser(thirdUser);
            long afterThirdSave = userClient.countUsers();
            Assertions.assertEquals(beforeSave + 3, afterThirdSave);
            Assertions.assertEquals(afterFirstSave + 2, afterThirdSave);
            Assertions.assertEquals(afterSecondSave + 1, afterThirdSave);
        } finally {
            destroy();
        }
    }

    @Test
    void deleteUserByIdIntegrationTest() {
        try {
            init();
            long countBeforeSave = userClient.countUsers();
            userClient.saveUser(firstUser);
            userClient.saveUser(secondUser);
            userClient.saveUser(thirdUser);
            long countAfterSaveAll = userClient.countUsers();
            Assertions.assertEquals(countBeforeSave + 3, countAfterSaveAll);
            userClient.deleteUserById(firstUser.getId());
            long countAfterFirstDelete = userClient.countUsers();
            Assertions.assertEquals(countAfterFirstDelete + 1, countAfterSaveAll);
            userClient.deleteUserById(secondUser.getId());
            long countAfterSecondDelete = userClient.countUsers();
            Assertions.assertEquals(countAfterSecondDelete + 1, countAfterFirstDelete);
            Assertions.assertEquals(countAfterSecondDelete + 2, countAfterSaveAll);
        } finally {
            destroy();
        }
    }

}
