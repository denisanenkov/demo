package ru.iteco.at.center.api.client;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import ru.iteco.at.center.client.UserFeignClient;
import ru.iteco.at.center.model.dto.JobPlainDTO;
import ru.iteco.at.center.model.dto.UserDTO;

import java.util.*;

public class JobRestEndpointTest {

    private static final String PATH = "http://localhost:8080/";

    final IJobRestEndpoint jobRestEndpoint = IJobRestEndpoint.client(PATH);

    @Before
    public void init()  {
        jobRestEndpoint.deleteAllJobs();
    }

    @After
    public void clearAll() {
        jobRestEndpoint.deleteAllJobs();
    }

    @Test
    public void testSaveJob() {
        final JobPlainDTO jobPlainDTO = new JobPlainDTO("name", "buildNumber", "description");
        jobRestEndpoint.saveJob(jobPlainDTO);
        final JobPlainDTO jobPlainFromServer = jobRestEndpoint.findJobById(jobPlainDTO.getId());
        Assertions.assertEquals(jobPlainDTO.getName(), jobPlainFromServer.getName());
    }

    @Test
    public void testFindJobById() {
        final JobPlainDTO jobPlainDTO = new JobPlainDTO("name", "buildNumber", "description");
        jobRestEndpoint.saveJob(jobPlainDTO);
        final JobPlainDTO jobPlainFromServer = jobRestEndpoint.findJobById(jobPlainDTO.getId());
        Assertions.assertEquals(jobPlainDTO.getName(), jobPlainFromServer.getName());
    }

    @Test
    public void testFindJobByName() {
        final JobPlainDTO jobPlainDTO = new JobPlainDTO("name", "buildNumber", "description");
        jobRestEndpoint.saveJob(jobPlainDTO);
        final JobPlainDTO jobName = new JobPlainDTO();
        jobName.setName("name");
        final List<JobPlainDTO> jobList = jobRestEndpoint.findJobByName(jobName);
        Assertions.assertEquals(jobPlainDTO.getId(), jobList.get(0).getId());
    }

    @Test
    public void testExistJobById() {
        final JobPlainDTO jobPlainDTO = new JobPlainDTO("name", "buildNumber", "description");
        Assertions.assertFalse(jobRestEndpoint.existJobById(jobPlainDTO.getId()));
        jobRestEndpoint.saveJob(jobPlainDTO);
        Assertions.assertTrue(jobRestEndpoint.existJobById(jobPlainDTO.getId()));
    }

    @Test
    public void testDeleteJobById() {
        final JobPlainDTO jobPlainDTO = new JobPlainDTO("name", "buildNumber", "description");
        Assertions.assertFalse(jobRestEndpoint.existJobById(jobPlainDTO.getId()));
        jobRestEndpoint.saveJob(jobPlainDTO);
        Assertions.assertTrue(jobRestEndpoint.existJobById(jobPlainDTO.getId()));
        jobRestEndpoint.deleteJobById(jobPlainDTO.getId());
        Assertions.assertFalse(jobRestEndpoint.existJobById(jobPlainDTO.getId()));
    }

    @Test
    public void testFindAllJobs() {
        jobRestEndpoint.saveJob(new JobPlainDTO("name1", "buildNumber1", "description1"));
        jobRestEndpoint.saveJob(new JobPlainDTO("name2", "buildNumber2", "description2"));
        jobRestEndpoint.saveJob(new JobPlainDTO("name3", "buildNumber3", "description3"));
        Assertions.assertEquals(3, jobRestEndpoint.findAllJobs().size());
    }

    @Test
    public void testDeleteAllJobs() {
        jobRestEndpoint.saveJob(new JobPlainDTO("name1", "buildNumber1", "description1"));
        jobRestEndpoint.saveJob(new JobPlainDTO("name2", "buildNumber2", "description2"));
        jobRestEndpoint.saveJob(new JobPlainDTO("name3", "buildNumber3", "description3"));
        jobRestEndpoint.deleteAllJobs();
        Assertions.assertEquals(0, jobRestEndpoint.findAllJobs().size());
    }

}
