package ru.iteco.at.center.model.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;

@Data
@EqualsAndHashCode(of = "id", callSuper = true)
public class CategoryJobDTO extends AbstractEntityDTO implements Serializable {

    @Nullable
    private String jobId;

    @Nullable
    private String categoryId;
}
