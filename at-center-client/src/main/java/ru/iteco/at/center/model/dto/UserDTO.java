package ru.iteco.at.center.model.dto;

import lombok.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.iteco.at.center.enumerate.Role;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "id", callSuper = true)
public class UserDTO extends AbstractEntityDTO implements Serializable {

    @Nullable
    private String login;

    @Nullable
    private String password;

    private boolean locked;

    @Nullable
    private String token;

    @NotNull
    private Set<String> roles = new HashSet<>();

    @Nullable
    private String email;

    public boolean isAccountNonExpired() {
        return true;
    }

    public boolean isAccountNonLocked() {
        return true;
    }

    public boolean isCredentialsNonExpired() {
        return true;
    }

    public boolean isEnabled() {
        return true;
    }

    private String lastName;

    private String firstName;

    private String middleName;

    @Override
    public String toString() {
        return "\nUSER: " +
                "ID = '" + id + '\'' + "\n" +
                "LOGIN = '" + login + '\'' + "\n" +
                "PASSWORD = '" + password + '\'' + "\n" +
                "TOKEN = '" + token + '\'' + "\n" +
                "ROLES = " + roles + "\n" +
                "EMAIL = '" + email + '\'' + "\n" +
                "LAST NAME = '" + lastName + '\'' + "\n" +
                "FIRST NAME = '" + firstName + '\'' + "\n" +
                "MIDDLE NAME = '" + middleName + '\'' + "}\n\n";
    }

    public UserDTO(
            final @Nullable String login,
            final @Nullable String password,
            final @Nullable String token,
            final @NotNull Set<String> roles,
            final @Nullable String email,
            final @Nullable String lastName,
            final @Nullable String firstName,
            final @Nullable String middleName
    ) {
        this.login = login;
        this.password = password;
        this.token = token;
        this.roles = roles;
        this.email = email;
        this.lastName = lastName;
        this.firstName = firstName;
        this.middleName = middleName;
    }

    public UserDTO(
            final @Nullable String login,
            final @Nullable String password
    ) {
        this.login = login;
        this.password = password;
    }

}
