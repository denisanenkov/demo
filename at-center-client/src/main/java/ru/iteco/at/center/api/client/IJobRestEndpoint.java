package ru.iteco.at.center.api.client;

import feign.Feign;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.MediaType;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.iteco.at.center.model.dto.JobPlainDTO;

import java.util.List;

@RequestMapping("/api")
public interface IJobRestEndpoint {

    static IJobRestEndpoint client(final String baseUrl) {
        final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        final HttpMessageConverters converters = new HttpMessageConverters(converter);
        final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(IJobRestEndpoint.class, baseUrl);
    }

    @PostMapping(value = "/job/dto")
    void saveJob(
            @RequestBody @Nullable JobPlainDTO jobPlainDTO
    );

    @Nullable
    @GetMapping("/job/find/id/{id}")
    JobPlainDTO findJobById(
            @PathVariable(value = "id") String id
    );

    @NotNull
    @PostMapping("/job/find/name")
    List<JobPlainDTO> findJobByName(@RequestBody JobPlainDTO jobPlainDTO);

    @GetMapping(value = "/job/exists/id/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    boolean existJobById(
            @PathVariable(name = "id") String id
    );

    @DeleteMapping(value = "/job/{id}")
    void deleteJobById(
            @PathVariable(name = "id") @Nullable String id
    );

    @NotNull
    @GetMapping(value = "/jobs", produces = MediaType.APPLICATION_JSON_VALUE)
    List<JobPlainDTO> findAllJobs();

    @DeleteMapping(value = "/jobs")
    void deleteAllJobs();

}
