package ru.iteco.at.center.enumerate;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

public enum TypeMetrik implements Serializable {

    CURRENT_AMOUNT("Current amount", "amount"),
    TIME_INTERVALS("Time interval", "ms"),
    LOAD_AVERAGE("Load average", "Mb");

    @NotNull
    public final String label;

    @NotNull
    public final String measure;
    @NotNull
    public String getMeasure() {
        return measure;
    }
    @NotNull
    public String getLabel() {
        return label;
    }

    TypeMetrik(@NotNull String label,@NotNull String measure) {
        this.label = label;
        this.measure = measure;
    }
}
