package ru.iteco.at.center.api.client;

import feign.*;
import feign.codec.Decoder;
import feign.codec.Encoder;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import feign.slf4j.Slf4jLogger;
import ru.iteco.at.center.model.dto.JobDTO;

public interface JobFeignClient {

    static JobFeignClient client(final String baseUrl) {
        Decoder decoder = new JacksonDecoder();
        Encoder encoder = new JacksonEncoder();
        return Feign.builder()
                .contract(new Contract.Default())
                .encoder(encoder)
                .decoder(decoder)
                .logger(new Slf4jLogger(JobFeignClient.class))
                .logLevel(Logger.Level.FULL)
                .target(JobFeignClient.class, baseUrl);
    }

    @RequestLine("POST")
    @Headers({"Content-Type: application/json"})
    void post(JobDTO jobDTO);

    @RequestLine("POST")
    @Headers({"Content-Type: application/json"})
    void deleteJob(JobDTO jobDTO);
}
