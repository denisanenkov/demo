package ru.iteco.at.center.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "app_job")
@JsonIgnoreProperties(ignoreUnknown = true)
public class JobPlainDTO extends AbstractEntityDTO implements Serializable {

    @Nullable
    @Column(name = "name")
    private String name;

    @Nullable
    @Column(name = "buildnumber")
    private String buildNumber;

    @Nullable
    @Column(name = "description")
    private String description;

    @Override
    public String toString() {
        return "JobPlainDTO{" +
                "name='" + name + '\'' +
                ", buildNumber='" + buildNumber + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
