package ru.iteco.at.center.model.dto;

import lombok.Data;

@Data
public class AuthenticationRequestDTO {

    private String username;

    private String password;

}
