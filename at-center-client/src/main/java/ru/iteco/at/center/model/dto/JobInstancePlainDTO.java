package ru.iteco.at.center.model.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.iteco.at.center.enumerate.StatusJob;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = "id", callSuper = true)
public class JobInstancePlainDTO extends AbstractEntityDTO implements Serializable {

    public JobInstancePlainDTO(JobInstancePlainDTO jobInstanceDTOPlain) {
        this.id = jobInstanceDTOPlain.getId();
        this.jobId = jobInstanceDTOPlain.getJobId();
        this.userId = jobInstanceDTOPlain.getUserId();
        this.systemId = jobInstanceDTOPlain.getSystemId();
        this.statusJob = jobInstanceDTOPlain.getStatusJob();
        this.dateBegin = jobInstanceDTOPlain.getDateBegin();
        this.dateEnd = jobInstanceDTOPlain.getDateEnd();
        this.memoryOfJob = jobInstanceDTOPlain.getMemoryOfJob();
    }

    @Nullable
    private String jobId;

    @Nullable
    private String userId;

    @Nullable
    private String systemId;

    @Nullable
    private StatusJob statusJob;

    @Nullable
    private Date dateBegin;

    @Nullable
    private Date dateEnd;

    @Nullable
    private String memoryOfJob;

}
