package ru.iteco.at.center.model.dto;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class CategoryDTO extends AbstractEntityDTO implements Serializable {

    @Nullable
    private String name;

    @Nullable
    private List<CategoryJobDTO> categoryJobList;

}