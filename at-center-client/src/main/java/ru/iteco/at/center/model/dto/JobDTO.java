package ru.iteco.at.center.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@EqualsAndHashCode(of = "id", callSuper = true)
public class JobDTO extends AbstractEntityDTO implements Serializable {

    public JobDTO(JobDTO jobDto) {
        this.id = jobDto.getId();
        this.userToken = jobDto.getUserToken();
        this.name = jobDto.getName();
        this.buildNumber = jobDto.getBuildNumber();
        this.jobInstanceDTO = new JobInstanceDTO(jobDto.getJobInstanceDTO());
    }

    @Nullable
    private String userToken;

    @Nullable
    private String name;

    @Nullable
    private String buildNumber;

    @Nullable
    private JobInstanceDTO jobInstanceDTO;

}
