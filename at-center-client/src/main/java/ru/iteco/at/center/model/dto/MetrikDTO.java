package ru.iteco.at.center.model.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.iteco.at.center.enumerate.TypeMetrik;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class MetrikDTO extends AbstractEntityDTO implements Serializable {

    public MetrikDTO(MetrikDTO metrikDto) {
        this.id = metrikDto.getId();
        this.jobInstanceId = metrikDto.getJobInstanceId();
        this.name = metrikDto.getName();
        this.type = metrikDto.getType();
        this.metrikValueDTOList = new ArrayList<>(metrikDto.getMetrikValueDTOList());
    }

    @Nullable
    private String jobInstanceId;

    @Nullable
    private String name;

    @Nullable
    private TypeMetrik type;

    @NotNull
    private List<MetrikValueDTO> metrikValueDTOList = new ArrayList<>();

}
