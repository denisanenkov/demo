package ru.iteco.at.center.model.dto;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.UUID;

@Getter
@Setter
@MappedSuperclass
public abstract class AbstractEntityDTO implements Serializable {

    @Id
    @NotNull
    @Column(name = "id")
    protected String id = UUID.randomUUID().toString();
}
