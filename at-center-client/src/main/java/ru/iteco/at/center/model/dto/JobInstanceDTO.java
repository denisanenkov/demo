package ru.iteco.at.center.model.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.iteco.at.center.enumerate.StatusJob;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = "id", callSuper = true)
public class JobInstanceDTO extends AbstractEntityDTO implements Serializable {

    public JobInstanceDTO(JobInstanceDTO jobInstanceDTO) {
        this.id = jobInstanceDTO.getId();
        this.jobId = jobInstanceDTO.getJobId();
        this.userId = jobInstanceDTO.getUserId();
        this.systemDTO = jobInstanceDTO.getSystemDTO();
        this.statusJob = jobInstanceDTO.getStatusJob();
        this.metrikDTOList = new ArrayList<>(jobInstanceDTO.getMetrikDTOList());
        this.dateBegin = jobInstanceDTO.getDateBegin();
        this.dateEnd = jobInstanceDTO.getDateEnd();
        this.memoryOfJob = jobInstanceDTO.getMemoryOfJob();
    }

    @Nullable
    private String jobId;

    @Nullable
    private String userId;

    @Nullable
    private SystemDTO systemDTO;

    @Nullable
    private StatusJob statusJob;

    @NotNull
    private List<MetrikDTO> metrikDTOList = new ArrayList<>();

    @Nullable
    private Date dateBegin;

    @Nullable
    private Date dateEnd;

    @Nullable
    private String memoryOfJob;

}
