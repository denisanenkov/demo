package ru.iteco.at.center.client;

import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.http.converter.FormHttpMessageConverter;
import ru.iteco.at.center.api.client.IUserRestEndpoint;
import org.springframework.beans.factory.ObjectFactory;
import ru.iteco.at.center.model.dto.UserDTO;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.NotNull;
import feign.Feign;

import java.util.List;
import java.util.Set;

public class UserFeignClient {

    private static final String PATH = "http://localhost:8080/";

    static IUserRestEndpoint client(final String baseUrl) {
        final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        final HttpMessageConverters converters = new HttpMessageConverters(converter);
        final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(IUserRestEndpoint.class, baseUrl);
    }

    @Nullable
    public List<UserDTO> findAllUsers() {
        return client(PATH).findAllUsers();
    }

    @Nullable
    public Set<UserDTO> findUsersByParameter(@NotNull final String parameter) {
        return client(PATH).findUsersByParameter(parameter);
    }

    public void saveUser(@Nullable final UserDTO user) {
        client(PATH).saveUser(user);
    }

    public void updateUser(@Nullable final UserDTO user) {
        client(PATH).updateUser(user);
    }

    public void deleteAllUsers() {
        client(PATH).deleteAllUsers();
    }

    public void deleteUserById(@Nullable final String id) {
        client(PATH).deleteUserById(id);
    }

    public long countUsers() {
        return client(PATH).countUsers();
    }

    public boolean existsByLogin(@NotNull final String login) {
        return client(PATH).existsByLogin(login);
    }

}
