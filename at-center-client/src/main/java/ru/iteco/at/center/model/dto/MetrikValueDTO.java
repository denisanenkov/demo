package ru.iteco.at.center.model.dto;

import lombok.*;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "id", callSuper = true)
public class MetrikValueDTO extends AbstractEntityDTO implements Serializable {

    public MetrikValueDTO(MetrikValueDTO metrikValueDto) {
        this.metrikId = metrikValueDto.getMetrikId();
        this.value = metrikValueDto.getValue();
        this.timeStamp = metrikValueDto.getTimeStamp();
    }

    @Nullable
    private String metrikId;

    @Nullable
    private String value;

    @Nullable
    private Date timeStamp;

}
