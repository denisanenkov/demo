package ru.iteco.at.center.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;
import ru.iteco.at.center.api.repository.JobInstanceRepository;
import ru.iteco.at.center.api.repository.JobRepository;
import ru.iteco.at.center.api.repository.MetrikRepository;
import ru.iteco.at.center.api.repository.MetrikValueRepository;
import ru.iteco.at.center.api.rest.JobController;
import ru.iteco.at.center.api.service.JobInstanceService;
import ru.iteco.at.center.api.service.JobService;
import ru.iteco.at.center.api.service.SystemService;
import ru.iteco.at.center.api.service.UserService;
import ru.iteco.at.center.configuration.WebAppConfig;
import ru.iteco.at.center.model.dto.JobDTO;
import ru.iteco.at.center.model.dto.JobInstanceDTO;
import ru.iteco.at.center.model.dto.MetrikValueDTO;
import ru.iteco.at.center.model.entity.Job;
import ru.iteco.at.center.model.entity.JobInstance;
import ru.iteco.at.center.model.entity.User;
import ru.iteco.at.center.util.ConvertEntityToDto;
import ru.iteco.at.center.util.TestDataUtil;

import java.util.Date;
import java.util.Random;

import static org.assertj.core.api.Assertions.assertThat;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {WebAppConfig.class})
@WebAppConfiguration
class JobControllerImplTest {

    private static final String USER_TOKEN = "7b8yitgm893423895c09238kx74s";
    private static final String USER_LOGIN = "h2test";
    private static final String NEW_JOB_ID = "test-new-job-123-321";
    private static final String EXIST_JOB_ID = "test-exist-job-123-321";
    private final Random random = new Random();

    @Autowired
    private WebApplicationContext webApplicationContext;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private JobController jobController;

    @Autowired
    private UserService userService;
    @Autowired
    private JobService jobService;
    @Autowired
    private SystemService systemService;
    @Autowired
    private JobInstanceService jobInstanceService;

    @Autowired
    private JobRepository jobRepository;
    @Autowired
    private JobInstanceRepository jobInstanceRepository;
    @Autowired
    private MetrikRepository metrikRepository;
    @Autowired
    private MetrikValueRepository metrikValueRepository;

    private User user;

    private Job newJob;
    private JobDTO newJobDTO;
    private String newJobInstanceId;
    private JobInstance newJobInstance;
    private JobInstanceDTO newJobInstanceDTO;

    private Job existsJob;
    private JobDTO existsJobDTO;
    private String existsJobInstanceId;
    private JobInstance existsJobInstance;
    private JobInstanceDTO existsJobInstanceDTO;

    @BeforeAll
    void setUp() throws JsonProcessingException {
        this.user = userService.create(TestDataUtil.initUser(USER_LOGIN, USER_TOKEN));

        this.newJob = TestDataUtil.initNewJob(NEW_JOB_ID, "new job");
        this.newJobInstance = TestDataUtil.initJobInstance(user, newJob);
        this.newJobInstanceDTO = ConvertEntityToDto.jobInstanceToDto(newJobInstance);
        this.newJob.getJobInstanceList().add(newJobInstance);
        this.newJobDTO = ConvertEntityToDto.jobToDto(newJob, USER_TOKEN, newJobInstanceDTO);
        this.newJobInstanceId = newJobInstance.getId();

        this.existsJob = TestDataUtil.initNewJob(EXIST_JOB_ID, "exists job");
        this.existsJobInstance = TestDataUtil.initJobInstance(user, existsJob);
        this.existsJobInstanceId = existsJobInstance.getId();
        this.existsJob.getJobInstanceList().add(existsJobInstance);
        jobService.save(existsJob);

        this.existsJobInstanceDTO = ConvertEntityToDto.jobInstanceToDto(existsJobInstance);
        this.existsJobDTO = ConvertEntityToDto.jobToDto(existsJob, USER_TOKEN, existsJobInstanceDTO);
    }

    @Test
    void saveJobData() throws Exception {
        jobController.post(newJobDTO);
        final Job savedJob = jobRepository.findById(NEW_JOB_ID).get();
        final JobInstance jobInstance = jobInstanceRepository.findById(newJobInstanceId).get();

        assertThat(NEW_JOB_ID).isEqualTo(savedJob.getId());
        assertThat(newJob.getName()).isEqualTo(savedJob.getName());
        assertThat(newJob.getBuildNumber()).isEqualTo(savedJob.getBuildNumber());
        assertThat(newJobInstanceId).isEqualTo(jobInstance.getId());
        assertThat(newJobInstance.getDateBegin()).isEqualTo(jobInstance.getDateBegin());
        assertThat(newJobInstance.getDateEnd()).isEqualTo(jobInstance.getDateEnd());
        assertThat(newJobInstance.getStatusJob()).isEqualTo(jobInstance.getStatusJob());
        assertThat(newJobInstance.getOperationSystem()).isEqualToComparingFieldByField(jobInstance.getOperationSystem());
    }

    @Test
    void updateJobData() throws Exception {
        System.out.println("[ BEFORE ]:" + objectMapper.writeValueAsString(existsJobDTO));

        existsJobDTO.getJobInstanceDTO().getMetrikDTOList().forEach(metrikDTO -> {
            metrikDTO.getMetrikValueDTOList()
                    .add(new MetrikValueDTO(metrikDTO.getId(), String.valueOf(random.nextInt(100000)), new Date()));
        });
        existsJobDTO.setBuildNumber("2.0.0");
        System.out.println("[ REQUEST ]:" + objectMapper.writeValueAsString(existsJobDTO));

        jobController.post(existsJobDTO);
        final Job updatedJob = jobRepository.findById(EXIST_JOB_ID).get();
        final JobInstance jobInstance = jobInstanceRepository.findById(existsJobInstanceId).get();

        assertThat(EXIST_JOB_ID).isEqualTo(updatedJob.getId());
        assertThat(existsJobDTO.getName()).isEqualTo(updatedJob.getName());
        assertThat(existsJobDTO.getBuildNumber()).isEqualTo(updatedJob.getBuildNumber());
        assertThat(existsJobInstanceId).isEqualTo(jobInstance.getId());
        assertThat(existsJobInstance.getDateBegin()).isEqualTo(jobInstance.getDateBegin());
        assertThat(existsJobInstance.getDateEnd()).isEqualTo(jobInstance.getDateEnd());
        assertThat(existsJobInstance.getStatusJob()).isEqualTo(jobInstance.getStatusJob());
        assertThat(existsJobInstance.getOperationSystem()).isEqualToComparingFieldByField(jobInstance.getOperationSystem());
    }
}