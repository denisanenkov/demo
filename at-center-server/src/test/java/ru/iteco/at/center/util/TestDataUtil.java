package ru.iteco.at.center.util;

import lombok.experimental.UtilityClass;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.iteco.at.center.enumerate.StatusJob;
import ru.iteco.at.center.enumerate.SystemCapacity;
import ru.iteco.at.center.enumerate.TypeMetrik;
import ru.iteco.at.center.model.entity.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

@UtilityClass
public class TestDataUtil {

    private static final PasswordEncoder ENCODER = new BCryptPasswordEncoder();
    private static final String BUILD_NUMBER = "build-1.0.0";
    private static final String SYSTEM_NAME = "Windows";
    private static final String METRIK_NAME = "hdd";

    public static User initUser(String login, String token) {
        final User user = new User();
        user.setLogin(login);
        user.setPassword(ENCODER.encode(login));
        user.setToken(token);
        return user;
    }

    public static Job initNewJob(String jobId, String name) {
        final Job job = new Job();
        job.setId(jobId);
        job.setName(name);
        job.setBuildNumber(BUILD_NUMBER);
        return job;
    }

    public static JobInstance initJobInstance(User user, Job job) {
        final JobInstance jobInstance = new JobInstance();
        jobInstance.setJob(job);
        jobInstance.setUser(user);
        jobInstance.setDateBegin(new Date(119, 9, 5, 10, 0));
        jobInstance.setDateEnd(new Date(119, 9, 6, 17, 0));
        jobInstance.setStatusJob(StatusJob.PLANNED);
        jobInstance.setOperationSystem(initSystem());
        jobInstance.setMetrikList(initMetrikList(jobInstance));
        return jobInstance;
    }

    public static OperationSystem initSystem() {
        final OperationSystem operationSystem = new OperationSystem();
        operationSystem.setName(SYSTEM_NAME);
        operationSystem.setSystemCapacity(SystemCapacity.CAPACITY_32);
        return operationSystem;
    }

    public static List<Metrik> initMetrikList(JobInstance jobInstance) {
        final List<Metrik> metrikList = new ArrayList<>();
        final Metrik metrik = new Metrik();
        metrik.setName(METRIK_NAME);
        metrik.setJobInstance(jobInstance);
        metrik.setType(TypeMetrik.CURRENT_AMOUNT);
        metrik.setValues(initListMetrikValue(metrik));
        metrikList.add(metrik);
        return metrikList;
    }

    public static List<MetrikValue> initListMetrikValue(Metrik metrik) {
        final Random random = new Random();
        final List<MetrikValue> metrikValueList = new ArrayList<>();
        final IntStream intStream = random.ints(10, 1000, 100000);
        intStream.forEach(value -> {
            final MetrikValue metrikValue = new MetrikValue();
            metrikValue.setMetrik(metrik);
            metrikValue.setTimeStamp(new Date(119, 9, 5, random.nextInt(24), 0));
            metrikValue.setValue(String.valueOf(value));
            metrikValueList.add(metrikValue);
        });
        return metrikValueList;
    }

}
