package ru.iteco.at.center.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.iteco.at.center.api.repository.JobRepository;
import ru.iteco.at.center.api.service.JobService;
import ru.iteco.at.center.exception.JobNotFoundException;
import ru.iteco.at.center.model.entity.Job;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import static java.util.Comparator.naturalOrder;

@Service
@Transactional
public class JobServiceImpl implements JobService {

    @Autowired
    private JobRepository jobRepository;

    @NotNull
    public Job save(@NotNull final Job job) {
        return jobRepository.save(job);
    }

    @NotNull
    public Job findById(@NotNull final String id) {
        return jobRepository.findById(id).orElseThrow(JobNotFoundException::new);
    }

    @NotNull
    @Override
    public List<Job> findByName(@NotNull final String searchString) {
        return jobRepository.findAllByNameContainingIgnoreCaseOrderByName(searchString).stream()
                .sorted(Comparator.nullsLast(Comparator
                        .comparing(Job::getName, Comparator.nullsLast(naturalOrder()))))
                .collect(Collectors.toList());
    }

    public boolean existsById(@NotNull final String id) {
        return jobRepository.existsById(id);
    }

    public void deleteById(@NotNull final String id) {
        jobRepository.deleteById(id);
    }

    public void delete(@NotNull final Job entity) {
        jobRepository.delete(entity);
    }

    @NotNull
    public List<Job> findAll() {
        return jobRepository.findAllByOrderByNameAsc().stream()
                .sorted(Comparator.nullsLast(Comparator
                        .comparing(Job::getName, Comparator.nullsLast(naturalOrder()))))
                .collect(Collectors.toList());
    }

    @Override
    public void deleteAll() {
        jobRepository.deleteAll();
    }

}
