package ru.iteco.at.center.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.iteco.at.center.api.repository.MetrikRepository;
import ru.iteco.at.center.api.service.MetrikService;
import ru.iteco.at.center.enumerate.TypeMetrik;
import ru.iteco.at.center.exception.MetrikNotFoundException;
import ru.iteco.at.center.model.entity.Metrik;

import java.util.List;

@Service
public class MetrikServiceImpl implements MetrikService {

    @Autowired
    private MetrikRepository metrikRepository;

    @NotNull
    @Transactional
    public Metrik save(@NotNull final Metrik metrik) {
        return metrikRepository.save(metrik);
    }

    @NotNull
    public List<Metrik> saveAll(@NotNull final Iterable<Metrik> iterable) {
        return metrikRepository.saveAll(iterable);
    }

    @NotNull
    public Metrik findById(@NotNull final String id) {
        return metrikRepository.findById(id).orElseThrow(MetrikNotFoundException::new);
    }

    @NotNull
    public Iterable<Metrik> findAllByJobInstanceId(@NotNull final String jobInstanceId) {
        return metrikRepository.findAllByJobInstanceId(jobInstanceId);
    }

    public boolean existsById(@NotNull final String id) {
        return metrikRepository.existsById(id);
    }

    @Transactional
    public void delete(@NotNull final Metrik metrik) {
        metrikRepository.delete(metrik);
    }

    @Transactional
    public void deleteById(@NotNull final String id) {
        metrikRepository.deleteById(id);
    }

    @Transactional
    public void deleteAll(@NotNull final Iterable<Metrik> entities) {
        metrikRepository.deleteAll(entities);
    }

    @Transactional
    public void deleteAllByJobInstanceId(@NotNull final String jobInstanceId) {
        metrikRepository.deleteAllByJobInstanceId(jobInstanceId);
    }

    public boolean existsByJobInstanceId(@NotNull final String jobInstanceId) {
        return metrikRepository.existsByJobInstanceId(jobInstanceId);
    }

    @NotNull
    @Override
    public Metrik findByJobInstanceIdAndType(@NotNull final String id, @NotNull final TypeMetrik typeMetrik) {
        @NotNull final Metrik metrik = metrikRepository.findByJobInstanceIdAndType(id, typeMetrik)
                .orElseThrow(MetrikNotFoundException::new);
        return metrik;
    }
}
