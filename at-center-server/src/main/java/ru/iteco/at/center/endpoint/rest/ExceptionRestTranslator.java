package ru.iteco.at.center.endpoint.rest;

import com.sun.istack.NotNull;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import ru.iteco.at.center.security.filter.ExceptionHandlerFilter;

import java.util.HashMap;
import java.util.Map;

/**
 * Handles exceptions from {@link ExceptionHandlerFilter}
 *
 */

@RestControllerAdvice
public class ExceptionRestTranslator {

    @ExceptionHandler({Exception.class})
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public Map<String, Object> handleError(Exception exception) {
        @NotNull final Map<String, Object> map = new HashMap<>();
        map.put("exception_message", exception.getMessage());
        map.put("http_status", HttpStatus.INTERNAL_SERVER_ERROR);
        return map;
    }

}