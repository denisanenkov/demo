package ru.iteco.at.center.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.iteco.at.center.enumerate.Role;
import ru.iteco.at.center.exception.ConversionException;
import ru.iteco.at.center.model.dto.*;
import ru.iteco.at.center.model.entity.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public final class ConvertEntityToDto {

    private ConvertEntityToDto() {
        throw new IllegalStateException("Utility class");
    }

    public static CategoryDTO categoryToDto(@NotNull final Category category) {
        @NotNull CategoryDTO categoryDTO = new CategoryDTO();
        categoryDTO.setId(category.getId());
        categoryDTO.setName(category.getName());
        categoryDTO.setCategoryJobList(
                category.getCategoryJobList() != null ?
                        category.getCategoryJobList()
                                .stream()
                                .map(ConvertEntityToDto::categoryJobToDto)
                                .collect(Collectors.toList())
                        : null);
        return categoryDTO;
    }

    public static CategoryJobDTO categoryJobToDto(@NotNull final CategoryJob categoryJob) {
        @NotNull CategoryJobDTO categoryJobDTO = new CategoryJobDTO();
        categoryJobDTO.setId(categoryJob.getId());
        categoryJobDTO.setCategoryId(categoryJob.getCategory().getId());
        categoryJobDTO.setJobId(categoryJob.getJob().getId());
        return categoryJobDTO;
    }

    public static JobDTO jobToDto(@NotNull final Job job, @NotNull final String userToken, @NotNull final JobInstanceDTO jobInstanceDTO) {
        @NotNull JobDTO jobDTO = new JobDTO();
        jobDTO.setId(job.getId());
        jobDTO.setName(job.getName());
        jobDTO.setUserToken(userToken);
        jobDTO.setBuildNumber(job.getBuildNumber());
        jobDTO.setJobInstanceDTO(jobInstanceDTO);
        return jobDTO;
    }

    public static JobInstanceDTO jobInstanceToDto(@NotNull final JobInstance jobInstance) {
        @NotNull final JobInstanceDTO jobInstanceDTO = new JobInstanceDTO();
        jobInstanceDTO.setId(jobInstance.getId());
        @Nullable final Job job = jobInstance.getJob();
        if (job == null) throw new ConversionException();
        jobInstanceDTO.setJobId(job.getId());
        @Nullable final User user = jobInstance.getUser();
        if (user == null) throw new ConversionException();
        jobInstanceDTO.setUserId(user.getId());
        @Nullable final OperationSystem operationSystem = jobInstance.getOperationSystem();
        if (operationSystem == null) throw new ConversionException();
        jobInstanceDTO.setSystemDTO(operationSystemToDto(operationSystem));
        jobInstanceDTO.setStatusJob(jobInstance.getStatusJob());
        jobInstanceDTO.setMetrikDTOList(jobInstance.getMetrikList()
                .stream()
                .map(ConvertEntityToDto::metrikToDto)
                .collect(Collectors.toList()));
        jobInstanceDTO.setDateBegin(jobInstance.getDateBegin());
        jobInstanceDTO.setDateEnd(jobInstance.getDateEnd());
        jobInstanceDTO.setMemoryOfJob(jobInstance.getMemoryOfJob());
        return jobInstanceDTO;
    }

    public static JobInstancePlainDTO jobInstanceToDtoPlain(@NotNull final JobInstance jobInstance) {
        @NotNull final JobInstancePlainDTO jobInstanceDTOPlain = new JobInstancePlainDTO();
        jobInstanceDTOPlain.setId(jobInstance.getId());
        @Nullable final Job job = jobInstance.getJob();
        jobInstanceDTOPlain.setJobId(job == null ? null : job.getId());
        @Nullable final User user = jobInstance.getUser();
        jobInstanceDTOPlain.setUserId(user == null ? null : user.getId());
        @Nullable final OperationSystem operationSystem = jobInstance.getOperationSystem();
        jobInstanceDTOPlain.setSystemId(operationSystem == null ? null : operationSystem.getId());
        jobInstanceDTOPlain.setStatusJob(jobInstance.getStatusJob());
        jobInstanceDTOPlain.setDateBegin(jobInstance.getDateBegin());
        jobInstanceDTOPlain.setDateEnd(jobInstance.getDateEnd());
        jobInstanceDTOPlain.setMemoryOfJob(jobInstance.getMemoryOfJob());
        return jobInstanceDTOPlain;
    }

    public static MetrikDTO metrikToDto(@NotNull final Metrik metrik) {
        @NotNull MetrikDTO metrikDTO = new MetrikDTO();
        metrikDTO.setId(metrik.getId());
        metrikDTO.setJobInstanceId(metrik.getJobInstance().getId());
        metrikDTO.setName(metrik.getName());
        metrikDTO.setType(metrik.getType());
        if (!metrik.getValues().isEmpty()) {
            metrikDTO.setMetrikValueDTOList(metrik.getValues().
                    stream().map(ConvertEntityToDto::metrikValueToDto).
                    collect(Collectors.toList()));
        }
        return metrikDTO;
    }

    public static MetrikValueDTO metrikValueToDto(@NotNull final MetrikValue metrikValue) {
        @NotNull MetrikValueDTO metrikValueDTO = new MetrikValueDTO();
        metrikValueDTO.setId(metrikValue.getId());
        metrikValueDTO.setMetrikId(metrikValue.getMetrik().getId());
        metrikValueDTO.setValue(metrikValue.getValue());
        metrikValueDTO.setTimeStamp(metrikValue.getTimeStamp());
        return metrikValueDTO;
    }

    public static SystemDTO operationSystemToDto(@NotNull final OperationSystem operationSystem) {
        if (operationSystem.getId() == null || operationSystem.getId().isEmpty()) return null;
        if (operationSystem.getName() == null || operationSystem.getName().isEmpty()) return null;
        if (operationSystem.getSystemCapacity() == null) return null;
        @NotNull SystemDTO systemDTO = new SystemDTO();
        systemDTO.setId(operationSystem.getId());
        systemDTO.setName(operationSystem.getName());
        systemDTO.setDescription(operationSystem.getDescription());
        systemDTO.setSystemCapacity(operationSystem.getSystemCapacity());
        return systemDTO;
    }

    @NotNull
    private static Set<String> toStringSetRole(@NotNull final Set<Role> roleList) {
        @NotNull Set<String> rolesList = new HashSet<>();
        for (@NotNull final Role role : roleList) {
            rolesList.add(role.toString());
        }
        return rolesList;
    }

    @NotNull
    public static UserDTO toUserDTO(@NotNull final User user) {
        @NotNull UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setLogin(user.getLogin());
        userDTO.setPassword(user.getPassword());
        userDTO.setLocked(user.isLocked());
        userDTO.setEmail(user.getEmail());
        userDTO.setFirstName(user.getFirstName());
        userDTO.setMiddleName(user.getMiddleName());
        userDTO.setLastName(user.getLastName());
        userDTO.setToken(user.getToken());
        userDTO.setRoles(toStringSetRole(user.getRoles()));
        return userDTO;
    }

    @NotNull
    public static List<UserDTO> toUserDTOList(@NotNull final List<User> users) {
        @NotNull List<UserDTO> userDTOList = new ArrayList<>();
        for (@NotNull final User user : users) {
            userDTOList.add(toUserDTO(user));
        }
        return userDTOList;
    }

    @NotNull
    public static List<JobDTO> toJobDTOList(@NotNull final List<Job> jobs) {
        @NotNull List<JobDTO> jobDTOList = new ArrayList<>();
        for (@NotNull final Job job : jobs) {
            jobDTOList.add(jobToDto(job, "", null));
        }
        return jobDTOList;
    }

}
