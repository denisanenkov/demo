package ru.iteco.at.center.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import ru.iteco.at.center.api.rest.JobController;
import ru.iteco.at.center.api.service.*;
import ru.iteco.at.center.exception.InvalidUserTokenException;
import ru.iteco.at.center.exception.JobDTOIsNullException;
import ru.iteco.at.center.model.dto.*;
import ru.iteco.at.center.model.entity.*;
import ru.iteco.at.center.util.ConvertDtoToEntity;

@RestController
@RequestMapping("/job")
public class JobControllerImpl implements JobController {

    @Autowired
    private JobService jobService;

    @Autowired
    private TokenService tokenService;

    @Override
    @Transactional
    @PostMapping(consumes = "application/json")
    public void post(@RequestBody JobDTO jobDTO) {
        try {
            if (jobDTO == null) throw new JobDTOIsNullException();
            if (!tokenService.validateToken(jobDTO.getUserToken()))
                throw new InvalidUserTokenException(jobDTO.getUserToken());
            final Job job = new ConvertDtoToEntity().parseJobFromDTO(jobDTO);
            jobService.save(job);
        } catch (JobDTOIsNullException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage(), ex);
        } catch (InvalidUserTokenException ex) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, ex.getMessage(), ex);
        } catch (RuntimeException ex) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage(), ex);
        }
    }

    @Override
    @Transactional
    @PostMapping(path = "/deleteJob", consumes = "application/json")
    public void deleteJob(@RequestBody JobDTO jobDTO) {
        try {
            if (jobDTO == null) throw new JobDTOIsNullException();
            if (!tokenService.validateToken(jobDTO.getUserToken()))
                throw new InvalidUserTokenException(jobDTO.getUserToken());
            final Job job = new ConvertDtoToEntity().parseJobFromDTO(jobDTO);
            jobService.delete(job);
        } catch (JobDTOIsNullException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage(), ex);
        } catch (InvalidUserTokenException ex) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, ex.getMessage(), ex);
        } catch (RuntimeException ex) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage(), ex);
        }
    }

}
