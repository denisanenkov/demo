package ru.iteco.at.center.api.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.iteco.at.center.model.entity.Category;
import ru.iteco.at.center.model.entity.Job;

import java.util.List;

@Repository
public interface JobRepository extends JpaRepository<Job, String> {

    List<Job> findAllByOrderByNameAsc();

    List<Job> findAllByNameContainingIgnoreCaseOrderByName(@NotNull final String searchString);

}
