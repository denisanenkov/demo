package ru.iteco.at.center.model.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.iteco.at.center.enumerate.TypeMetrik;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "app_metrik")
@NoArgsConstructor
@EqualsAndHashCode(of = "id", callSuper = true)
public class Metrik extends AbstractEntity implements Serializable {

    public Metrik(JobInstance jobInstance) {
        this.jobInstance = jobInstance;
    }

    @Nullable
    @ManyToOne
    @JoinColumn(name = "jobinstance_id")
    private JobInstance jobInstance;

    @Nullable
    private String name;

    @Nullable
    @Enumerated(EnumType.STRING)
    private TypeMetrik type;

    @NotNull
    @OneToMany(fetch = FetchType.LAZY,
            mappedBy = "metrik",
            orphanRemoval = true,
            cascade = {CascadeType.PERSIST})
    private List<MetrikValue> values = new ArrayList<>();
}
