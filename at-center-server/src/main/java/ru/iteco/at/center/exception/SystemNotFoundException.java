package ru.iteco.at.center.exception;

public class SystemNotFoundException extends RuntimeException {
    public SystemNotFoundException() {
        super("OperationSystem not found.");
    }
}
