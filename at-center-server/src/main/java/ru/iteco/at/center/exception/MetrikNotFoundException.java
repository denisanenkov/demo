package ru.iteco.at.center.exception;

public class MetrikNotFoundException extends RuntimeException {
    public MetrikNotFoundException() {
        super("Metrik not found.");
    }
}
