package ru.iteco.at.center.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.iteco.at.center.api.repository.JobInstanceRepository;
import ru.iteco.at.center.api.service.JobInstanceService;
import ru.iteco.at.center.exception.JobInstanceNotFoundException;
import ru.iteco.at.center.model.dto.JobInstancePlainDTO;
import ru.iteco.at.center.model.entity.JobInstance;
import ru.iteco.at.center.util.ConvertDtoToEntity;

import java.util.Collection;

@Service
public class JobInstanceServiceImpl implements JobInstanceService {

    @Autowired
    private JobInstanceRepository jobInstanceRepository;

    @NotNull
    @Override
    public Collection<JobInstance> findAllByJobId(@NotNull final String jobId) {
        return jobInstanceRepository.findAllByJobId(jobId);
    }

    @NotNull
    @Override
    public Collection<JobInstance> findAllByOperationSystemId(@NotNull final String systemId) {
        return jobInstanceRepository.findAllByOperationSystemId(systemId);
    }

    @NotNull
    @Override
    public Collection<JobInstance> findAllByUserId(@NotNull final String userId) {
        return jobInstanceRepository.findAllByUserId(userId);
    }

    @NotNull
    @Override
    public JobInstance save(@NotNull final JobInstance job) {
        return jobInstanceRepository.save(job);
    }

    @NotNull
    @Override
    public JobInstance save(@NotNull final JobInstancePlainDTO job) {
        return jobInstanceRepository.save(new ConvertDtoToEntity().parseJobInstanceFromDTOPlain(job));
    }

    @NotNull
    @Override
    public JobInstance findById(@NotNull final String id) {
        return jobInstanceRepository.findById(id).orElseThrow(JobInstanceNotFoundException::new);
    }

    @NotNull
    @Override
    public JobInstance findByIdAndUserId(@NotNull final String id, @NotNull final String jobId) {
        return jobInstanceRepository.findByIdAndJobId(id, jobId).orElseThrow(JobInstanceNotFoundException::new);
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return jobInstanceRepository.existsById(id);
    }

    @Override
    public boolean existsByJobId(@NotNull String jobId) {
        return jobInstanceRepository.existsByJobId(jobId);
    }

    @Override
    public void deleteById(@NotNull final String id) {
        jobInstanceRepository.deleteById(id);
    }

    @Override
    public void delete(@NotNull final JobInstance jobInstance) {
        jobInstanceRepository.delete(jobInstance);
    }

    @NotNull
    @Override
    public Collection<JobInstance> findAll() {
        return jobInstanceRepository.findAll();
    }

    @Override
    public void deleteAll() {
        jobInstanceRepository.deleteAll();
    }

}
