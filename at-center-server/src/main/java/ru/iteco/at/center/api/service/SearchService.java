package ru.iteco.at.center.api.service;

import org.jetbrains.annotations.NotNull;
import ru.iteco.at.center.model.entity.User;

import java.util.List;

public interface SearchService {
    @NotNull List<String> getUserFieldPrettyName();

    @NotNull List<User> searchUserByString(@NotNull String[] prettyNameUserFields,
                                           @NotNull String searchString);
}
