package ru.iteco.at.center.faces;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import ru.iteco.at.center.api.service.CategoryJobService;
import ru.iteco.at.center.api.service.CategoryService;
import ru.iteco.at.center.api.service.JobService;
import ru.iteco.at.center.model.entity.Category;
import ru.iteco.at.center.model.entity.CategoryJob;
import ru.iteco.at.center.model.entity.Job;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@SessionScoped
@ManagedBean(name = "categoryBean")
public class CategoryManagedBean extends SpringBeanAutowiringSupport {

    private static final String PRETTY_CATEGORY_CREATE = "pretty:createCategory";
    private static final String PRETTY_CATEGORY_EDIT = "pretty:editCategory";
    private static final String PRETTY_CATEGORY_VIEW = "pretty:viewCategory";
    private static final String PRETTY_CATEGORY_LIST = "pretty:categoryList";
    private static final String PRETTY_CATEGORY_PROFILE_VIEW = "pretty:viewCategoryProfile";

    @Autowired
    private JobService jobService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private CategoryJobService categoryJobService;

    private String searchString;

    @NotNull
    private Category category = new Category();

    private List<Category> list = new ArrayList<>();

    @NotNull
    public String getEditCategoryPage(final @NotNull Category category) {
        this.category = category;
        return PRETTY_CATEGORY_EDIT;
    }

    public void prepareEditCategoryView() {
        final String id = FacesContext
                .getCurrentInstance()
                .getExternalContext()
                .getRequestParameterMap()
                .get("id");
        this.category = categoryService.findById(id);
    }

    @NotNull
    public String getCreateCategoryPage() {
        this.category = new Category();
        return PRETTY_CATEGORY_CREATE;
    }

    @NotNull
    public String editCategory() {
        categoryService.edit(category);
        list = (List<Category>) categoryService.findAll();
        return PRETTY_CATEGORY_LIST;
    }

    @NotNull
    public String createCategory() {
        categoryService.create(category);
        category = new Category();
        list = (List<Category>) categoryService.findAll();
        return PRETTY_CATEGORY_LIST;
    }

    @NotNull
    public String viewCategory(@NotNull final Category category) {
        this.category = category;
        return PRETTY_CATEGORY_VIEW;
    }

    @NotNull
    public String viewCategoryProfile(@NotNull final Category category) {
        this.category = category;
        return PRETTY_CATEGORY_PROFILE_VIEW;
    }

    @NotNull
    public String categories() {
        searchString = "";
        list = (List<Category>) categoryService.findAll();
        return PRETTY_CATEGORY_LIST;
    }

    @NotNull
    public List<Category> search() {
        list = (List<Category>) categoryService.searchByName(searchString);
        return list;
    }

    @NotNull
    public List<Category> resetSearch() {
        searchString = "";
        return (List<Category>) categoryService.findAll();
    }

    public void setCategoryList(@NotNull final List<Category> categoryList) {
        this.list = categoryList;
    }

    public boolean jobsInCategoryExists() {
        return categoryJobService.existsByCategoryId(category.getId());
    }

    public Iterable<Job> jobsOfCategory() {
        return categoryJobService.findAllJobByCategoryId(category.getId());
    }

    public String deleteCategory(@NotNull final Category category) {
        categoryService.delete(category);
        list.remove(category);
        return PRETTY_CATEGORY_LIST;
    }

    @NotNull
    public String addJob(Category category, Job job) {
        CategoryJob categoryJob = new CategoryJob();
        categoryJob.setCategory(category);
        categoryJob.setJob(job);
        categoryJobService.addJob(categoryJob);
        return PRETTY_CATEGORY_VIEW;
    }

}
