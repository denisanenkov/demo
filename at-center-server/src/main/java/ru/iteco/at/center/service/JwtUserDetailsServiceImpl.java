package ru.iteco.at.center.service;

import com.sun.istack.NotNull;
import com.sun.istack.Nullable;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.iteco.at.center.api.service.UserService;
import ru.iteco.at.center.model.entity.User;
import ru.iteco.at.center.security.jwt.JwtUser;
import ru.iteco.at.center.security.jwt.JwtUserFactory;

/**
 * Implementation of {@link UserDetailsService} interface for {@link JwtUser}.
 *
 */

@Service
@Slf4j
public class JwtUserDetailsServiceImpl implements UserDetailsService {

    private final UserService userService;

    @Autowired
    public JwtUserDetailsServiceImpl(UserService userService) {
        this.userService = userService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        @Nullable final User user = userService.findByLogin(username);
        if (user == null) throw new UsernameNotFoundException("User with username: " + username + " not found");
        @NotNull final JwtUser jwtUser = JwtUserFactory.create(user);
        log.info("IN loadUserByUsername - user with username: {} successfully loaded", username);
        return jwtUser;
    }

}
