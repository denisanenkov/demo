package ru.iteco.at.center.api.rest;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.iteco.at.center.model.dto.JobDTO;

@RestController
@RequestMapping("/job")
public interface JobController {

    @PostMapping(consumes = "application/json")
    void post(@RequestBody JobDTO jobDTO);

    @RequestMapping(value = "/deleteJob")
    @PostMapping(consumes = "application/json")
    void deleteJob(@RequestBody JobDTO jobDTO);
}
