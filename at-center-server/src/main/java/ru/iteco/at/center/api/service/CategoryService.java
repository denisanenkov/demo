package ru.iteco.at.center.api.service;

import org.jetbrains.annotations.NotNull;
import ru.iteco.at.center.model.entity.Category;

public interface CategoryService {

    void create(@NotNull Category category);

    void edit(@NotNull Category category);

    @NotNull
    Category findById(@NotNull String id);

    boolean existsById(@NotNull String id);

    void deleteById(@NotNull String id);

    void delete(@NotNull Category category);

    void deleteAll(@NotNull Iterable<? extends Category> categories);

    @NotNull
    Iterable<Category> findAll();

    @NotNull
    Iterable<Category> searchByName(@NotNull final String searchString);

}
