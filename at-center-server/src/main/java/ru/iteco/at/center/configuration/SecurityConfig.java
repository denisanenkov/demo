package ru.iteco.at.center.configuration;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.EnableGlobalAuthentication;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.transaction.annotation.Transactional;
import ru.iteco.at.center.api.repository.UserRepository;
import ru.iteco.at.center.enumerate.Role;
import ru.iteco.at.center.model.entity.User;
import ru.iteco.at.center.security.exception.handler.RestAccessDeniedHandler;
import ru.iteco.at.center.security.exception.handler.RestAuthenticationEntryPoint;
import ru.iteco.at.center.security.filter.ExceptionHandlerFilter;
import ru.iteco.at.center.security.jwt.JwtConfigurer;
import ru.iteco.at.center.security.jwt.JwtTokenProvider;

import javax.annotation.PostConstruct;
import java.util.HashSet;
import java.util.Set;

@Configuration
@EnableWebSecurity
@ComponentScan(basePackages = "ru.iteco.at.center")
@EnableGlobalAuthentication
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true, jsr250Enabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private static final String ADMIN = "admin";
    private static final String ADMIN_UP_CASE = "ADMIN";
    private static final String ADMIN_MAIL = "admin@admin.admin";


    private static final String TEST = "test";
    private static final String TEST_MAIL = "test@test.test";
    private static final String TEST_TOKEN = "$2a$10$.z8RxKjzYgn0Twlg8yphTeFe4pLozgCNSMCm4GubuoekbAOAa2OOK";


    private static final String USER = "user";
    private static final String USER_UP_CASE = "USER";
    private static final String USER_MAIL = "user@user.user";


    private static final String TEMP_ADMIN = "tempadmin";
    private static final String TEMP_ADMIN_MAIL = "tempadmin@admin.admin";

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Autowired
    private ExceptionHandlerFilter exceptionHandlerFilter;

    @Override
    protected void configure(@NotNull AuthenticationManagerBuilder auth) throws Exception {

        final PasswordEncoder encoder = passwordEncoder();

        auth.userDetailsService(userDetailsService)
                .passwordEncoder(encoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.authorizeRequests()
                .and().formLogin().loginPage("/login").failureUrl("/login?error=true").defaultSuccessUrl("/", true)
                .and().logout().permitAll().logoutSuccessUrl("/login")
                .invalidateHttpSession(true)
                .deleteCookies("JSESSIONID")
                .and()
                .addFilterBefore(exceptionHandlerFilter, UsernamePasswordAuthenticationFilter.class)
                .exceptionHandling()
                .accessDeniedHandler(new RestAccessDeniedHandler())
                .authenticationEntryPoint(new RestAuthenticationEntryPoint())
                .and()
                .apply(new JwtConfigurer(jwtTokenProvider))
                .and().csrf().disable();

        http.authorizeRequests()
                .antMatchers("/", "/registration", "/javax.faces.resource/**", "/resources/**").permitAll()
                .antMatchers("/job**", "/metrik**", "/category**", "/system**").hasAnyAuthority(ADMIN_UP_CASE, USER_UP_CASE)
                .antMatchers("/profile", "/change-password", "/documentation").hasAnyAuthority(ADMIN_UP_CASE, USER_UP_CASE)
                .antMatchers("/user**", "/settings").hasAnyAuthority(ADMIN_UP_CASE)
                .antMatchers("/user**").hasAnyAuthority(ADMIN_UP_CASE)
                .antMatchers("/api/auth/login").permitAll()
//                .antMatchers("/api/**").fullyAuthenticated();
                .antMatchers("/api/**").permitAll();
    }

    @Transactional
    @PostConstruct
    public void initDefaultUsers() {
        final User admin = new User();
        admin.setLogin(ADMIN);
        admin.setEmail(ADMIN_MAIL);
        @NotNull final Set<Role> adminRoles = new HashSet<>();
        adminRoles.add(Role.USER);
        adminRoles.add(Role.ADMIN);
        admin.setRoles(adminRoles);
        admin.setPassword(passwordEncoder().encode(ADMIN));
        if (!userRepository.existsByLogin(admin.getLogin())) userRepository.save(admin);

        final User user = new User();
        user.setLogin(USER);
        user.setEmail(USER_MAIL);
        @NotNull final Set<Role> userRoles = new HashSet<>();
        userRoles.add(Role.USER);
        user.setRoles(userRoles);
        user.setPassword(passwordEncoder().encode(ADMIN));
        if (!userRepository.existsByLogin(user.getLogin())) userRepository.save(user);

        final User test = new User();
        test.setLogin(TEST);
        test.setEmail(TEST_MAIL);
        @NotNull final Set<Role> testRoles = new HashSet<>();
        testRoles.add(Role.USER);
        test.setRoles(testRoles);
        test.setPassword(passwordEncoder().encode(ADMIN));
        test.setToken(TEST_TOKEN);
        if (!userRepository.existsByLogin(test.getLogin())) userRepository.save(test);

        final User tempAdmin = new User();
        tempAdmin.setLogin(TEMP_ADMIN);
        tempAdmin.setEmail(TEMP_ADMIN_MAIL);
        @NotNull final Set<Role> tempAdminRoles = new HashSet<>();
        tempAdminRoles.add(Role.ADMIN);
        tempAdmin.setRoles(tempAdminRoles);
        tempAdmin.setPassword(passwordEncoder().encode(ADMIN));
        if (!userRepository.existsByLogin(tempAdmin.getLogin())) userRepository.save(tempAdmin);
    }
}
