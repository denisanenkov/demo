package ru.iteco.at.center.api.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.iteco.at.center.model.entity.Job;

import java.util.List;

@Service
@Transactional
public interface JobService {
    @NotNull
    Job save(@NotNull final Job job);

    @NotNull
    Job findById(@NotNull final String id);

    @NotNull
    List<Job> findAll();

    boolean existsById(@NotNull final String id);

    void deleteById(@NotNull final String id);

    void delete(@NotNull final Job entity);

    @NotNull
    Iterable<Job> findByName(@NotNull final String searchString);

    void deleteAll();
}
