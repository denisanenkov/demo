package ru.iteco.at.center.endpoint.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.iteco.at.center.api.service.JobInstanceService;
import ru.iteco.at.center.model.dto.JobInstancePlainDTO;
import ru.iteco.at.center.model.entity.JobInstance;
import ru.iteco.at.center.util.ConvertEntityToDto;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class JobInstanceRestEndpoint {

    @NotNull
    @Autowired
    private JobInstanceService jobInstanceService;

    @NotNull
    @GetMapping(value = "/job/instances", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<JobInstancePlainDTO> findJobInstanceAll() {
        @NotNull final Collection<JobInstance> jobInstances = jobInstanceService.findAll();
        return jobInstances.stream()
                .map(ConvertEntityToDto::jobInstanceToDtoPlain)
                .collect(Collectors.toList());
    }

    @NotNull
    @GetMapping(value = "/job/instances/find/job_id/{job_id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<JobInstancePlainDTO> findJobInstanceByJobIdAll(
            @NotNull @PathVariable(name = "job_id") final String jobId
    ) {
        @NotNull final Collection<JobInstance> jobInstances = jobInstanceService.findAllByJobId(jobId);
        return jobInstances.stream()
                .map(ConvertEntityToDto::jobInstanceToDtoPlain)
                .collect(Collectors.toList());
    }

    @NotNull
    @GetMapping(value = "/job/instances/find/os_id/{os_id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<JobInstancePlainDTO> findJobInstanceByOperationSystemIdAll(
            @NotNull @PathVariable(name = "os_id") final String osId
    ) {
        @NotNull final Collection<JobInstance> jobInstances = jobInstanceService.findAllByOperationSystemId(osId);
        return jobInstances.stream()
                .map(ConvertEntityToDto::jobInstanceToDtoPlain)
                .collect(Collectors.toList());
    }

    @NotNull
    @GetMapping(value = "/job/instances/find/user_id/{user_id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<JobInstancePlainDTO> findJobInstanceByUserIdAll(
            @NotNull @PathVariable(name = "user_id") final String userId
    ) {
        @NotNull final Collection<JobInstance> jobInstances = jobInstanceService.findAllByUserId(userId);
        return jobInstances.stream()
                .map(ConvertEntityToDto::jobInstanceToDtoPlain)
                .collect(Collectors.toList());
    }

    @DeleteMapping(value = "/job/instances/all")
    public void deleteJobInstanceAll() {
        jobInstanceService.deleteAll();
    }

    @Nullable
    @GetMapping(value = "/job/instance/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public JobInstancePlainDTO findJobInstanceById(
            @NotNull @PathVariable(name = "id") final String id
    ) {
        return ConvertEntityToDto.jobInstanceToDtoPlain(jobInstanceService.findById(id));
    }

    @DeleteMapping(value = "/job/instance/{id}")
    public void deleteJobInstanceById(
            @NotNull @PathVariable(name = "id") final String id
    ) {
        jobInstanceService.deleteById(id);
    }

    @Nullable
    @RequestMapping(
            method = {RequestMethod.POST, RequestMethod.PUT},
            value = "/job/instance/", produces = MediaType.APPLICATION_JSON_VALUE
    )
    public JobInstancePlainDTO save(@NotNull @RequestBody final JobInstancePlainDTO jobInstancePlainDTO) {
        return ConvertEntityToDto.jobInstanceToDtoPlain(jobInstanceService.save(jobInstancePlainDTO));
    }

    @GetMapping(value = "/job/instance/exists/id/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public boolean existsById(@NotNull @PathVariable("id") final String id) {
        return jobInstanceService.existsById(id);
    }

    @GetMapping(value = "/job/instance/exists/job_id/{job_id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public boolean existsByJobId(@NotNull @PathVariable("job_id") final String jobId) {
        return jobInstanceService.existsByJobId(jobId);
    }

}
