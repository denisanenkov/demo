package ru.iteco.at.center.exception;

public class JobInstanceNotFoundException extends RuntimeException {

    public JobInstanceNotFoundException() {
        super("JobInstance not found.");
    }
}
