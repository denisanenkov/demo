package ru.iteco.at.center.exception;

public class EmptyEntityException extends RuntimeException {

    public EmptyEntityException(Class classEnt) {
        super("Error! Entity " + classEnt.getName() + " is Empty!");
    }

    public EmptyEntityException() {
        super("Error! Entity is empty..");
    }

}
