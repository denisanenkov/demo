package ru.iteco.at.center.faces;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import ru.iteco.at.center.api.service.UserService;
import ru.iteco.at.center.enumerate.Role;
import ru.iteco.at.center.exception.DuplicateUserException;
import ru.iteco.at.center.model.entity.User;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import java.util.*;

@Getter
@Setter
@SessionScoped
@ManagedBean(name = "adminBean")
public class AdminManagedBean extends SpringBeanAutowiringSupport {

    private static final String PRETTY_HOME = "pretty:index";
    private static final String PRETTY_SETTINGS = "pretty:settings";
    private static final String PRETTY_USER_MANAGEMENT = "pretty:userManagement";
    private static final String PRETTY_EDIT = "pretty:editUser";
    private static final String PRETTY_PROFILE_VIEW = "pretty:viewProfileUser";
    private static final String PRETTY_LOGOUT = "pretty:logoutUser";

    @Nullable
    private User userEdit;

    @NotNull
    private User user = new User();

    @Autowired
    private UserService userService;

    @NotNull
    private List<Role> roles = Arrays.asList(Role.values());

    private String[] selectedRoles;

    @NotNull
    private List<User> list = new ArrayList<>();

    public String list() {
        list = userService.findAll();
        return PRETTY_USER_MANAGEMENT;
    }

    public String viewSettings() {
        return PRETTY_SETTINGS;
    }

    public String saveSettings() {
        return PRETTY_HOME;
    }

    //Не баг, все методы нужны, Exception скорей всего тоже
    public void validateUserEditLogin(
            @NotNull final FacesContext context,
            @NotNull final UIComponent component,
            @NotNull final Object value
    ) throws ValidatorException {
        @NotNull final String login = String.valueOf(value);
        if (userEdit == null) return;
        @NotNull final User userOriginal = userService.findById(userEdit.getId());
        boolean isUserEditLogin = !login.equals(userOriginal.getLogin());
        boolean isDuplicateLogin = userService.findByLogin(login) != null;
        if (isUserEditLogin && isDuplicateLogin) {
            @NotNull final FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_WARN,
                    new DuplicateUserException().getMessage(), null);
            throw new ValidatorException(facesMessage);
        }
    }

    private User getCurrentUser() {
        this.user = userService.findLoggedUser();
        return user;
    }

    public boolean hasRole(@NotNull final String checkedRole) {
        @NotNull final Role role = Role.valueOf(checkedRole);
        return getCurrentUser().getRoles().contains(role);
    }

    public void deleteUser(@NotNull final User user) {
        userService.deleteUser(user);
        list.remove(user);
    }

    @NotNull
    public String editUser(@NotNull final User user) {
        prepareEditUser(user);
        return PRETTY_EDIT;
    }

    @NotNull
    public String viewProfileUser(@NotNull final User user) {
        prepareEditUser(user);
        return PRETTY_PROFILE_VIEW;
    }

    public void viewEditUser() {
        final String id = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("id");
        this.userEdit = userService.findById(id);
    }

    @NotNull
    public String saveUserEdit(@NotNull final User user) {
        @NotNull final Set<Role> editRoleSet = new HashSet<>();
        for (@NotNull final String roleName : selectedRoles) {
            editRoleSet.add(Role.valueOf(roleName));
        }
        boolean isCurrentUser = Objects.equals(userService.findLoggedUser().getId(), user.getId());
        boolean isChangedLogin = !Objects.equals(userService.findLoggedUser().getLogin(), user.getLogin());
        user.setRoles(editRoleSet);
        userService.saveUser(user);
        if (isCurrentUser && isChangedLogin) return PRETTY_LOGOUT;
        return list();
    }

    private void prepareEditUser(@NotNull final User user) {
        this.userEdit = user;
        @NotNull final List<String> userRolesName = new ArrayList<>();
        user.getRoles().forEach(role -> userRolesName.add(role.name()));
        selectedRoles = new String[userRolesName.size()];
        selectedRoles = userRolesName.toArray(selectedRoles);
    }

}
