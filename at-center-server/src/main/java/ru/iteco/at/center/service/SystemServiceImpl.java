package ru.iteco.at.center.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.iteco.at.center.api.repository.SystemRepository;
import ru.iteco.at.center.api.service.SystemService;
import ru.iteco.at.center.enumerate.SystemCapacity;
import ru.iteco.at.center.exception.SystemNotFoundException;
import ru.iteco.at.center.model.entity.OperationSystem;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Comparator.naturalOrder;

@Service
@Transactional
public class SystemServiceImpl implements SystemService {

    @Autowired
    private SystemRepository systemRepository;

    @NotNull
    public OperationSystem save(@NotNull final OperationSystem operationSystem) {
        @Nullable final OperationSystem system = findOneByNameAndCapacity(
                operationSystem.getName(),
                operationSystem.getSystemCapacity()
        );
        if (system != null) return system;
        return systemRepository.save(operationSystem);
    }

    @NotNull
    public OperationSystem findById(@NotNull final String id) {
        return systemRepository.findById(id).orElseThrow(SystemNotFoundException::new);
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return systemRepository.existsById(id);
    }

    public void deleteById(@NotNull final String id) {
        systemRepository.deleteById(id);
    }

    public void deleteAll() {
        systemRepository.deleteAll();
    }

    @NotNull
    @Override
    public Iterable<OperationSystem> findAll() {
        return systemRepository.findAllByOrderByNameAsc().stream()
                .sorted(Comparator.nullsLast(Comparator
                        .comparing(OperationSystem::getName, Comparator.nullsLast(naturalOrder()))))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public Iterable<OperationSystem> findAllByJobInstanceId(@NotNull final String id) {
        return systemRepository.findAllByOrderByNameAsc().stream()
                .sorted(Comparator.nullsLast(Comparator
                        .comparing(OperationSystem::getName, Comparator.nullsLast(naturalOrder()))))
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    public OperationSystem findOneByNameAndCapacity(
            @Nullable final String name,
            @Nullable final SystemCapacity systemCapacity
    ) {
        if (name == null || name.isEmpty()) return null;
        if (systemCapacity == null) return null;
        return systemRepository.findOneByNameAndCapacity(name, systemCapacity);
    }

    @NotNull
    @Override
    public List<OperationSystem> searchByName(@NotNull final String searchString) {
        return systemRepository.findAllByNameContainingIgnoreCaseOrderByName(searchString).stream()
                .sorted(Comparator.nullsLast(Comparator
                        .comparing(OperationSystem::getName, Comparator.nullsLast(naturalOrder()))))
                .collect(Collectors.toList());
    }

    @Override
    public List<OperationSystem> findAllByNameAndCapacity(
            @Nullable final String name,
            @Nullable final SystemCapacity systemCapacity
    ) {
        if (name == null || name.isEmpty()) return new ArrayList<>(0);
        if (systemCapacity == null) return new ArrayList<>(0);
        return systemRepository.findAllByNameAndCapacity(name, systemCapacity);
    }

}
