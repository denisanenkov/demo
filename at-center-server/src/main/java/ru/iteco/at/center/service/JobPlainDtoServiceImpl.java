package ru.iteco.at.center.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.iteco.at.center.api.repository.JobPlainDtoRepository;
import ru.iteco.at.center.api.repository.JobRepository;
import ru.iteco.at.center.api.service.JobService;
import ru.iteco.at.center.exception.JobNotFoundException;
import ru.iteco.at.center.model.dto.JobPlainDTO;
import ru.iteco.at.center.model.entity.Job;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Comparator.naturalOrder;

@Service
@Transactional
public class JobPlainDtoServiceImpl {

    @Autowired
    private JobPlainDtoRepository jobPlainDtoRepository;

    @NotNull
    public JobPlainDTO save(@NotNull final JobPlainDTO job) {
        return jobPlainDtoRepository.save(job);
    }

    @NotNull
    public JobPlainDTO findById(@NotNull final String id) {
        return jobPlainDtoRepository.findById(id).orElseThrow(JobNotFoundException::new);
    }

    @NotNull
    public List<JobPlainDTO> findByName(@NotNull final String name) {
        return jobPlainDtoRepository.findAllByName(name);
    }

    public boolean existsById(@NotNull final String id) {
        return jobPlainDtoRepository.existsById(id);
    }

    public void deleteById(@NotNull final String id) {
        jobPlainDtoRepository.deleteById(id);
    }

    public void delete(@NotNull final JobPlainDTO job) {
        jobPlainDtoRepository.delete(job);
    }

    @NotNull
    public List<JobPlainDTO> findAll() {
        return jobPlainDtoRepository.findAllByOrderByNameAsc();
    }

    public void deleteAll() {
        jobPlainDtoRepository.deleteAll();
    }

}
