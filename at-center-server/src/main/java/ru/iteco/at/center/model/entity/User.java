package ru.iteco.at.center.model.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import ru.iteco.at.center.api.annotation.SearchStringFieldMarker;
import ru.iteco.at.center.exception.EmptyEntityException;
import ru.iteco.at.center.model.dto.UserDTO;
import ru.iteco.at.center.enumerate.Role;
import ru.iteco.at.center.exception.EmptyRoleSetException;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Setter
@Getter
@Entity
@Table(name = "app_user")
@EqualsAndHashCode(of = "id", callSuper = true)
public class User extends AbstractEntity implements UserDetails, Serializable {

    @Nullable
    @Column(unique = true)
    @SearchStringFieldMarker(prettyName = "Login")
    private String login;

    @Nullable
    private String password;

    @Nullable
    private String token;

    @Column(nullable=false, columnDefinition = "BOOLEAN DEFAULT FALSE")
    private boolean locked = false;

    @NotNull
    @ElementCollection(targetClass = Role.class, fetch = FetchType.EAGER)
    @CollectionTable(name = "app_user_role", joinColumns = @JoinColumn(name = "user_id"))
    @Enumerated(EnumType.STRING)
    private Set<Role> roles = new HashSet<>();

    @Nullable
    @SearchStringFieldMarker(prettyName = "Email")
    private String email;

    @NotNull
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user", orphanRemoval = true)
    private List<JobInstance> jobInstance = new ArrayList<>();

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<SimpleGrantedAuthority> authorityList = new java.util.ArrayList<>();
        getRoles().forEach(role -> authorityList.add(new SimpleGrantedAuthority(role.name())));
        return authorityList;
    }

    @Nullable
    @Override
    public String getPassword() {
        return password;
    }

    @Nullable
    @Override
    public String getUsername() {
        return login;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return !locked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Nullable
    @Column(name = "lastName")
    @SearchStringFieldMarker(prettyName = "Last name")
    private String lastName;

    @Nullable
    @Column(name = "firstName")
    @SearchStringFieldMarker(prettyName = "First name")
    private String firstName;

    @Nullable
    @Column(name = "middleName")
    @SearchStringFieldMarker(prettyName = "Middle name")
    private String middleName;

    @NotNull
    public static Set<Role> getRoleSet(@NotNull final Set<String> userDTORoles) {
        if (userDTORoles == null || userDTORoles.isEmpty()) throw new EmptyRoleSetException();
        @NotNull Set<Role> roles = new HashSet<>();
        for (@NotNull final String currentRole : userDTORoles) {
            switch (currentRole) {
                case ("ADMIN"):
                    roles.add(Role.ADMIN);
                    break;
                case ("USER"):
                    roles.add(Role.USER);
                    break;
                case ("GUEST"):
                    roles.add(Role.GUEST);
                    break;
            }
        }
        return roles;
    }

    @NotNull
    public static User toUser(@Nullable final UserDTO userDTO) {
        if (userDTO == null) throw new EmptyEntityException();
        @NotNull User user = new User();
        user.setId(userDTO.getId());
        user.setLogin(userDTO.getLogin());
        user.setPassword(userDTO.getPassword());
        user.setEmail(userDTO.getEmail());
        user.setFirstName(userDTO.getFirstName());
        user.setMiddleName(userDTO.getMiddleName());
        user.setLastName(userDTO.getLastName());
        user.setToken(userDTO.getToken());
        user.setRoles(getRoleSet(userDTO.getRoles()));
        return user;
    }

}
