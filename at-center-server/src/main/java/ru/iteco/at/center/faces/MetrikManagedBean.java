package ru.iteco.at.center.faces;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.primefaces.model.charts.line.LineChartModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import ru.iteco.at.center.api.service.MetrikService;
import ru.iteco.at.center.api.service.MetrikValueService;
import ru.iteco.at.center.api.service.UserService;
import ru.iteco.at.center.model.entity.Metrik;
import ru.iteco.at.center.model.entity.MetrikValue;
import ru.iteco.at.center.util.GraphsBuilderUtil;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@SessionScoped
@ManagedBean(name = "metrikBean")
public class MetrikManagedBean extends SpringBeanAutowiringSupport {

    @Autowired
    private MetrikService metrikService;

    @Autowired
    private MetrikValueService metrikValueService;

    @Autowired
    private UserService userService;

    @NotNull
    private Metrik metrik = new Metrik();

    @NotNull
    private Iterable<MetrikValue> metricValues = new ArrayList<>();

    public String viewMetric(@NotNull final Metrik metrik) {
        this.metrik = metrik;
        metricValues = metrikValueService.findAllByMetrikId(metrik.getId());
        return "pretty:viewMetrik";
    }

    @NotNull
    public Iterable<Metrik> list(@NotNull final String jobId) {
        return metrikService.findAllByJobInstanceId(jobId);
    }

    public boolean valuesExists() {
        return metrikValueService.existsByMetrikId(metrik.getId());
    }

    public void deleteMetric(@NotNull final Metrik metrik) {
        metrikService.delete(metrik);
    }

    @NotNull
    private List<MetrikValue> metricValuesSortByTimeStamp() {
        return metrikValueService.findAllByMetrikIdOrderByTimeStamp(metrik.getId());
    }

    @NotNull
    public LineChartModel getLineModel() {
        @NotNull final List<Number> timeStamps = new ArrayList<>();
        @NotNull final List<Object> metricValueList = new ArrayList<>();
        final Date metricDateBegin = metrik.getJobInstance().getDateBegin();
        metricValuesSortByTimeStamp().forEach(metricValue -> {
            final Long timeStamp = metricValue.getTimeStamp().getTime() - metricDateBegin.getTime();
            timeStamps.add(timeStamp);
            metricValueList.add(metricValue.getValue());
        });
        @NotNull final String timeStampLabel = "Time spent with beginning of job, ms";
        @NotNull final String metricValuesLabel = metrik.getName() + ", " + metrik.getType().getMeasure();
        @NotNull final String metricGraphLabel = metrik.getName() + " parameter graph";
        @NotNull final LineChartModel metricGraphModel = GraphsBuilderUtil.buildLineGraphModel(timeStamps,
                metricValueList, timeStampLabel, metricValuesLabel, metricGraphLabel);
        return metricGraphModel;
    }

    public int sortValues(@NotNull final Object obj1, @NotNull final Object obj2){
        final int id1 = Integer.parseInt((String)obj1);
        final int id2 = Integer.parseInt((String)obj2);
        return Integer.compare(id1, id2);
    }

}
