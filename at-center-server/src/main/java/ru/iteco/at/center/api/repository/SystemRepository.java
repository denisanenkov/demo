package ru.iteco.at.center.api.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.iteco.at.center.enumerate.SystemCapacity;
import ru.iteco.at.center.model.entity.OperationSystem;

import java.util.List;

@Repository
public interface SystemRepository extends JpaRepository<OperationSystem, String> {

    @Query(value = "SELECT a FROM OperationSystem a WHERE a.name=:name AND a.systemCapacity=:systemCapacity")
    OperationSystem findOneByNameAndCapacity(
            @Param("name") @NotNull final String name,
            @Param("systemCapacity") @NotNull final SystemCapacity systemCapacity
    );

    @Query(value = "SELECT a FROM OperationSystem a WHERE a.name=:name AND a.systemCapacity=:systemCapacity")
    List<OperationSystem> findAllByNameAndCapacity(
            @Param("name") @NotNull final String name,
            @Param("systemCapacity") @NotNull final SystemCapacity systemCapacity
    );

    List<OperationSystem> findAllByOrderByNameAsc();

    List<OperationSystem> findAllByNameContainingIgnoreCaseOrderByName(@NotNull final String searchString);

}
