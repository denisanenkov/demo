package ru.iteco.at.center.exception;

public class ConversionException extends RuntimeException {

    public ConversionException() {
        super("Error! Field is null...");
    }

}
