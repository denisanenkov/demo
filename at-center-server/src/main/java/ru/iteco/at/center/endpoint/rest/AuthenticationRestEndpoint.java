package ru.iteco.at.center.endpoint.rest;

import com.sun.istack.NotNull;
import com.sun.istack.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.iteco.at.center.api.service.UserService;
import ru.iteco.at.center.model.dto.AuthenticationRequestDTO;
import ru.iteco.at.center.model.entity.User;
import ru.iteco.at.center.security.jwt.JwtTokenProvider;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(value = "/auth/")
public class AuthenticationRestEndpoint {

    private final AuthenticationManager authenticationManager;

    private final JwtTokenProvider jwtTokenProvider;

    private final UserService userService;

    @Autowired
    public AuthenticationRestEndpoint(
            AuthenticationManager authenticationManager,
            JwtTokenProvider jwtTokenProvider,
            UserService userService
    ) {
        this.authenticationManager = authenticationManager;
        this.jwtTokenProvider = jwtTokenProvider;
        this.userService = userService;
    }

    @PostMapping("login")
    public ResponseEntity<Map<Object, Object>> login(
            @RequestBody @Nullable final AuthenticationRequestDTO requestDto
    ) {
        try {
            if (requestDto == null) throw new BadCredentialsException("nullable requestDTO");
            @Nullable final String username = requestDto.getUsername();
            if (username == null || username.isEmpty()) throw new BadCredentialsException("username is null");
            @Nullable final User user = userService.findByLogin(username);
            if (user == null)
                throw new UsernameNotFoundException("User with username: " + username + " not found");
            authenticationManager
                    .authenticate(new UsernamePasswordAuthenticationToken(username, requestDto.getPassword()));
            @NotNull final String token = jwtTokenProvider.createToken(username, user.getRoles());
            @NotNull final Map<Object, Object> response = new HashMap<>();
            response.put("username", username);
            response.put("token", token);
            return ResponseEntity.ok(response);
        } catch (AuthenticationException e) {
            @NotNull final Map<Object, Object> response = new HashMap<>();
            if (requestDto != null) response.put("username", requestDto.getUsername());
            response.put("status", e.getMessage());
            return ResponseEntity.status(403).body(response);
        }
    }

}
