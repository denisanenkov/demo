package ru.iteco.at.center.configuration;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.request.RequestContextListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

public class AppInitializer implements WebApplicationInitializer {

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {

        final AnnotationConfigWebApplicationContext root;

        root = new AnnotationConfigWebApplicationContext();
        root.register(WebAppConfig.class);
        root.register(SecurityConfig.class);
        root.setServletContext(servletContext);

        servletContext.addListener(new ContextLoaderListener(root));
        servletContext.addListener(new RequestContextListener());

        ServletRegistration.Dynamic restServlet = servletContext
                .addServlet("rest", new DispatcherServlet());
        restServlet.addMapping("/api/*");
    }
}
