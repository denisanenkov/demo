package ru.iteco.at.center.util;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import ru.iteco.at.center.api.service.*;
import ru.iteco.at.center.enumerate.SystemCapacity;
import ru.iteco.at.center.model.dto.*;
import ru.iteco.at.center.model.entity.*;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Getter
@Setter
@ManagedBean
@SessionScoped
public class ConvertDtoToEntity extends SpringBeanAutowiringSupport {

    @Autowired
    private JobService jobService;

    @Autowired
    private UserService userService;

    @Autowired
    private JobInstanceService jobInstanceService;

    @Autowired
    private SystemService systemService;

    @Autowired
    private MetrikService metrikService;

    @Autowired
    private MetrikValueService metrikValueService;

    public Job parseJobFromDTO(@NotNull final JobDTO jobDTO) {
        final Job job;
        final String jobId = jobDTO.getId();
        final JobInstanceDTO jobInstanceDTO = jobDTO.getJobInstanceDTO();
        if (jobInstanceDTO == null) return null;

        @Nullable final SystemDTO systemDTO = jobInstanceDTO.getSystemDTO();
        if (systemDTO == null) return null;
        @Nullable final String nameSystem = systemDTO.getName();
        if (nameSystem == null) return null;
        @Nullable final SystemCapacity capacitySystem = systemDTO.getSystemCapacity();
        if (capacitySystem == null) return null;
        @Nullable final OperationSystem system = systemService.findOneByNameAndCapacity(nameSystem, capacitySystem);
        if (system != null) systemDTO.setId(system.getId());

        jobInstanceDTO.setUserId(userService.findIdByToken(jobDTO.getUserToken()));
        if (jobService.existsById(jobId)) job = jobService.findById(jobId);
        else {
            final Job newJob = new Job();
            newJob.setId(jobDTO.getId());
            newJob.setName(jobDTO.getName());
            job = jobService.save(newJob);
        }
        job.setBuildNumber(jobDTO.getBuildNumber());
        job.getJobInstanceList().add(parseJobInstanceFromDTO(jobInstanceDTO, job));
        return job;
    }

    private JobInstance parseJobInstanceFromDTO(@NotNull final JobInstanceDTO jobInstanceDTO,
                                                @NotNull final Job job) {
        final JobInstance jobInstance;
        if (jobInstanceService.existsById(jobInstanceDTO.getId())) jobInstance =
                jobInstanceService.findById(jobInstanceDTO.getId());
        else {
            final JobInstance newJobInstance = new JobInstance();
            newJobInstance.setId(jobInstanceDTO.getId());
            newJobInstance.setJob(job);
            newJobInstance.setUser(userService.findById(jobInstanceDTO.getUserId()));
            jobInstance = jobInstanceService.save(newJobInstance);
        }
        jobInstance.setDateBegin(jobInstanceDTO.getDateBegin());
        jobInstance.setOperationSystem(parseSystemFromDTO(jobInstanceDTO.getSystemDTO()));
        jobInstance.setStatusJob(jobInstanceDTO.getStatusJob());
        jobInstance.setDateEnd(jobInstanceDTO.getDateEnd());
        jobInstance.setMemoryOfJob(jobInstanceDTO.getMemoryOfJob());
        jobInstance.getMetrikList().addAll(parseMetrikListFromDTO(jobInstanceDTO.getMetrikDTOList(), jobInstance));
        return jobInstance;
    }

    private OperationSystem parseSystemFromDTO(@Nullable final SystemDTO systemDTO) {
        if (systemDTO == null) return null;
        OperationSystem operationSystem = systemService.findOneByNameAndCapacity(systemDTO.getName(), systemDTO.getSystemCapacity());

        if (operationSystem == null) {
            OperationSystem newOperationSystem = new OperationSystem();
            newOperationSystem.setId(systemDTO.getId());
            newOperationSystem.setName(systemDTO.getName());
            newOperationSystem.setSystemCapacity(systemDTO.getSystemCapacity());
            operationSystem = systemService.save(newOperationSystem);
        }
        return operationSystem;
    }

    private Collection<? extends Metrik> parseMetrikListFromDTO(@Nullable List<MetrikDTO> metrikDTOList, JobInstance jobInstance) {
        if (metrikDTOList == null) return new ArrayList<>();
        final List<Metrik> metriks = new ArrayList<>();
        metrikDTOList.forEach(metrikDTO -> metriks.add(parseMetrikFromDTO(metrikDTO, jobInstance)));
        return metriks;
    }

    private Metrik parseMetrikFromDTO(@Nullable final MetrikDTO metrikDTO, JobInstance jobInstance) {
        if (metrikDTO == null || jobInstance == null) return null;
        final Metrik metrik;
        if (metrikService.existsById(metrikDTO.getId())) metrik = metrikService.findById(metrikDTO.getId());
        else {
            final Metrik newMetrik = new Metrik();
            newMetrik.setJobInstance(jobInstance);
            newMetrik.setName(metrikDTO.getName());
            newMetrik.setType(metrikDTO.getType());
            newMetrik.setId(metrikDTO.getId());
            metrik = metrikService.save(newMetrik);
        }
        metrik.getValues().addAll(parseMetrikValueListFromDTO(metrikDTO.getMetrikValueDTOList(), metrik));
        return metrik;
    }

    private Collection<? extends MetrikValue> parseMetrikValueListFromDTO(@Nullable final List<MetrikValueDTO> metrikValueDTOList, Metrik metrik) {
        if (metrikValueDTOList == null) return new ArrayList<>();
        final List<MetrikValue> metrikValues = new ArrayList<>();
        metrikValueDTOList.forEach(metrikValueDTO -> metrikValues.add(parseMetrikValueFromDTO(metrikValueDTO, metrik)));
        return metrikValues;
    }

    private MetrikValue parseMetrikValueFromDTO(@Nullable final MetrikValueDTO metrikValueDTO, Metrik metrik) {
        if (metrikValueDTO == null) return null;
        final MetrikValue metrikValue;
        if (metrikService.existsById(metrikValueDTO.getId()))
            metrikValue = metrikValueService.findById(metrikValueDTO.getId());
        else {
            final MetrikValue newMetrikValue = new MetrikValue();
            newMetrikValue.setId(metrikValueDTO.getId());
            newMetrikValue.setValue(metrikValueDTO.getValue());
            newMetrikValue.setTimeStamp(metrikValueDTO.getTimeStamp());
            newMetrikValue.setMetrik(metrik);
            metrikValue = metrikValueService.save(newMetrikValue);
        }
        return metrikValue;
    }

    public JobInstance parseJobInstanceFromDTOPlain(@NotNull final JobInstancePlainDTO jobInstanceDTOPlain) {
        final JobInstance jobInstance;
        if (jobInstanceService.existsById(jobInstanceDTOPlain.getId())) jobInstance =
                jobInstanceService.findById(jobInstanceDTOPlain.getId());
        else {
            jobInstance = new JobInstance();
            jobInstance.setId(jobInstanceDTOPlain.getId());
            jobInstance.setUser(userService.findById(jobInstanceDTOPlain.getUserId()));
            @NotNull final Iterable<Metrik> metriks = metrikService.findAllByJobInstanceId(jobInstance.getId());
            @NotNull final List<Metrik> metrikList = new ArrayList<>();
            for (@Nullable final Metrik metrik : metriks) metrikList.add(metrik);
            jobInstance.setMetrikList(metrikList);
        }
        jobInstance.setDateBegin(jobInstanceDTOPlain.getDateBegin());
        @Nullable final String systemId = jobInstanceDTOPlain.getSystemId();
        if (!(systemId == null)) jobInstance.setOperationSystem(systemService.findById(systemId));
        jobInstance.setStatusJob(jobInstanceDTOPlain.getStatusJob());
        jobInstance.setDateEnd(jobInstanceDTOPlain.getDateEnd());
        jobInstance.setMemoryOfJob(jobInstanceDTOPlain.getMemoryOfJob());
        return jobInstance;
    }
}
