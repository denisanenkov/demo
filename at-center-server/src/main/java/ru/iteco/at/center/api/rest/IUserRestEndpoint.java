package ru.iteco.at.center.api.rest;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.iteco.at.center.model.dto.UserDTO;

import java.util.List;
import java.util.Set;

public interface IUserRestEndpoint {
    @Nullable
    @SneakyThrows
    @GetMapping(value = "/users", produces = MediaType.APPLICATION_JSON_VALUE)
    List<UserDTO> findAllUsers();

    @Nullable
    @SneakyThrows
    @GetMapping(value = "/users/{parameter}", produces = MediaType.APPLICATION_JSON_VALUE)
    Set<UserDTO> findUsersByParameter(
            @PathVariable(name = "parameter") @NotNull String parameter
    );

    @Nullable
    @SneakyThrows
    @RequestMapping("/users/name")
    Set<UserDTO> findUserByName(
            @RequestParam(value = "firstName", required = false, defaultValue = "") String firstName,
            @RequestParam(value = "middleName", required = false, defaultValue = "") String middleName,
            @RequestParam(value = "lastName", required = false, defaultValue = "") String lastName
    );

    @SneakyThrows
    @PostMapping(value = "/user")
    void saveUser(
            @RequestBody @Nullable UserDTO user
    );

    @SneakyThrows
    @PutMapping(value = "/user")
    void updateUser(
            @RequestBody @Nullable UserDTO user
    );

    @SneakyThrows
    @DeleteMapping(value = "/users")
    void deleteAllUsers();

    @SneakyThrows
    @DeleteMapping(value = "/user/{id}")
    void deleteUserById(
            @PathVariable(name = "id") @Nullable String id
    );

    @SneakyThrows
    @GetMapping(value = "/users/count")
    long countUsers();

    @SneakyThrows
    @GetMapping(value = "/users/exists/login/{login}")
    boolean existsByLogin(
            @PathVariable(name = "login") @NotNull String login
    );
}
