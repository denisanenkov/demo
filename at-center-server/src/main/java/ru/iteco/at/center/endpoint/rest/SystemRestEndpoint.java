package ru.iteco.at.center.endpoint.rest;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import ru.iteco.at.center.api.service.SystemService;
import ru.iteco.at.center.enumerate.SystemCapacity;
import ru.iteco.at.center.model.dto.SystemDTO;
import ru.iteco.at.center.model.entity.OperationSystem;
import ru.iteco.at.center.util.ConvertEntityToDto;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class SystemRestEndpoint {

    @NotNull
    @Autowired
    private SystemService systemService;

    @Nullable
    @SneakyThrows
    @GetMapping(value = "/systems", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<SystemDTO> findAll() {
        @NotNull final List<OperationSystem> systems = (List<OperationSystem>) systemService.findAll();
        return systems
                .stream()
                .map(ConvertEntityToDto::operationSystemToDto)
                .collect(Collectors.toList());
    }

    @Nullable
    @SneakyThrows
    @GetMapping(value = "/systems/find/name/", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<SystemDTO> findAllByName(
            @RequestParam(value = "name", required = false, defaultValue = "") final String name
    ) {
        @NotNull final List<OperationSystem> systems = (List<OperationSystem>) systemService.searchByName(name);
        return systems
                .stream()
                .map(ConvertEntityToDto::operationSystemToDto)
                .collect(Collectors.toList());
    }

    @Nullable
    @SneakyThrows
    @GetMapping(value = "/systems/find/value/", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<SystemDTO> findAllByNameAndSystem(
            @RequestParam(value = "name", required = false, defaultValue = "") @Nullable final String name,
            @RequestParam(value = "capacity", required = false, defaultValue = "") @Nullable final SystemCapacity capacity
    ) {
        @NotNull final List<OperationSystem> systems = (List<OperationSystem>) systemService.findAllByNameAndCapacity(name, capacity);
        return systems
                .stream()
                .map(ConvertEntityToDto::operationSystemToDto)
                .collect(Collectors.toList());
    }

    @Nullable
    @GetMapping(value = "/system/find/id/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public SystemDTO findById(
            @PathVariable(name = "id") @NotNull final String id
    ) {
        try {
            @Nullable final OperationSystem operationSystem = systemService.findById(id);
            return ConvertEntityToDto.operationSystemToDto(operationSystem);
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage(), ex);
        }
    }

    @SneakyThrows
    @PostMapping(value = "/system/create", produces = MediaType.APPLICATION_JSON_VALUE)
    public void saveSystem(
            @RequestBody @NotNull final SystemDTO systemDTO
    ) {
        systemService.save(OperationSystem.toSystem(systemDTO));
    }

    @SneakyThrows
    @DeleteMapping(value = "/system/delete/id/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteSystemById(
            @PathVariable(name = "id") @NotNull final String id
    ) {
        systemService.deleteById(id);
    }

    @SneakyThrows
    @DeleteMapping(value = "/systems/deleteAll", produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteAll(
    ) {
        systemService.deleteAll();
    }

    @SneakyThrows
    @GetMapping(value = "/system/exists/id/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public boolean existsById(
            @PathVariable(name = "id") @NotNull final String id
    ) {
        return systemService.existsById(id);
    }

}