package ru.iteco.at.center.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.iteco.at.center.enumerate.SystemCapacity;
import ru.iteco.at.center.model.entity.OperationSystem;

@Service
@Transactional
public interface SystemService {
    @NotNull
    OperationSystem save(@NotNull OperationSystem operationSystem);

    @Nullable
    OperationSystem findById(@NotNull String id);

    boolean existsById(@NotNull String id);

    void deleteById(@NotNull String id);

    void deleteAll();

    @NotNull
    Iterable<OperationSystem> findAll();

    @NotNull
    Iterable<OperationSystem> findAllByJobInstanceId(@NotNull String jobInstanceId);

    @Nullable
    OperationSystem findOneByNameAndCapacity(@NotNull String name, @NotNull SystemCapacity systemCapacity);

    @NotNull
    Iterable<OperationSystem> searchByName(@NotNull final String searchString);

    Iterable<OperationSystem> findAllByNameAndCapacity(@Nullable String name, @Nullable SystemCapacity systemCapacity);

}
