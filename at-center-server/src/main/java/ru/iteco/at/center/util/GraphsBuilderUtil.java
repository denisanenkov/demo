package ru.iteco.at.center.util;

import org.jetbrains.annotations.NotNull;
import org.primefaces.model.charts.ChartData;
import org.primefaces.model.charts.axes.cartesian.CartesianScaleLabel;
import org.primefaces.model.charts.axes.cartesian.CartesianScales;
import org.primefaces.model.charts.axes.cartesian.linear.CartesianLinearAxes;
import org.primefaces.model.charts.line.LineChartDataSet;
import org.primefaces.model.charts.line.LineChartModel;
import org.primefaces.model.charts.line.LineChartOptions;
import org.primefaces.model.charts.pie.PieChartDataSet;
import org.primefaces.model.charts.pie.PieChartModel;

import java.util.ArrayList;
import java.util.List;

public class GraphsBuilderUtil {

    private GraphsBuilderUtil() {
        throw new IllegalStateException("Utility class");
    }
    /**
     * <p>Данный метод возвращает заполненную модель LineChartModel для вывода линейного графика.
     * Документация по LineChartModel доступна по ссылке:</p>
     * https://primefaces.github.io/primefaces/8_0/#/components/charts?id=linechart
     * @param dataSetYAxes Данные по шкале Y
     * @param dataSetXAxes Данные по оси X
     * @param yAxesLabel Название оси Y
     * @param xAxesLabel Название оси X
     * @param graphLabel Название выводимого графика
     * @return LineChartModel org.primefaces.model.charts.line.LineChartModel
     */
    @NotNull
    public static LineChartModel buildLineGraphModel(@NotNull final List<Number> dataSetYAxes,
                                                     @NotNull final List<Object> dataSetXAxes,
                                                     @NotNull final String yAxesLabel,
                                                     @NotNull final String xAxesLabel,
                                                     @NotNull final String graphLabel) {
        @NotNull final LineChartModel lineModel = new LineChartModel();
        @NotNull final ChartData data = new ChartData();
        @NotNull final LineChartDataSet dataSet = new LineChartDataSet();
        @NotNull final LineChartOptions options = new LineChartOptions();
        @NotNull final CartesianScales cScales = new CartesianScales();

        @NotNull final CartesianLinearAxes yLinearAxes = new CartesianLinearAxes();
        yLinearAxes.setPosition("left");
        @NotNull final CartesianScaleLabel yScaleLabel = new CartesianScaleLabel();
        yScaleLabel.setLabelString(yAxesLabel);
        yScaleLabel.setDisplay(true);
        yLinearAxes.setScaleLabel(yScaleLabel);

        @NotNull final CartesianLinearAxes xLinearAxes = new CartesianLinearAxes();
        xLinearAxes.setPosition("bottom");
        @NotNull final CartesianScaleLabel xScaleLabel = new CartesianScaleLabel();
        xScaleLabel.setLabelString(xAxesLabel);
        xScaleLabel.setDisplay(true);
        xLinearAxes.setScaleLabel(xScaleLabel);
        cScales.addYAxesData(yLinearAxes);
        cScales.addXAxesData(xLinearAxes);
        options.setScales(cScales);

        dataSet.setData(dataSetYAxes);
        data.setLabels(dataSetXAxes);
        dataSet.setFill(false);
        dataSet.setLabel(graphLabel);
        dataSet.setBorderColor("rgb(0, 129, 194)");
        dataSet.setLineTension(0.1);
        data.addChartDataSet(dataSet);
        lineModel.setData(data);
        lineModel.setOptions(options);
        return lineModel;
    }

    /**
     * <p>Данный метод возвращает заполненную модель PieChartModel для вывода круговой диаграммы.
     * Документация по PieChartModel доступна по ссылке:</p>
     * https://primefaces.github.io/primefaces/8_0/#/components/charts?id=piechart
     * @param dataValues Количественное значение передаваемого параметра
     * @param dataLabels Описание передаваемого параметра
     * @return {@link PieChartModel}
     */
    @NotNull
    public static PieChartModel buildPieGraphModel(@NotNull final List<Number> dataValues,
                                                   @NotNull final List<Object> dataLabels

    ){
        @NotNull final PieChartModel pieModel = new PieChartModel();
        @NotNull final ChartData data = new ChartData();
        @NotNull final PieChartDataSet dataSet = new PieChartDataSet();
        @NotNull final List<String> bgColors = new ArrayList<>();
        bgColors.add(0,"rgb(176,196,222)");//#B0C4DE
        bgColors.add(1,"rgb(154,205,50)");//#9ACD32
        bgColors.add(2,"rgb(32,178,170)");//#20B2AA
        bgColors.add(3,"rgb(70,130,180)");//#4682B4
        bgColors.add(4,"rgb(30,144,255)");//#1E90FF
        bgColors.add(5,"rgb(147,112,219)");//#9370DB
        bgColors.add(6,"rgb(255,228,196)");//#FFE4C4
        bgColors.add(7,"rgb(255,99,71)");//#FF6347
        dataSet.setData(dataValues);
        dataSet.setBackgroundColor(bgColors);
        data.addChartDataSet(dataSet);
        data.setLabels(dataLabels);
        pieModel.setData(data);
        return pieModel;
    }
}
