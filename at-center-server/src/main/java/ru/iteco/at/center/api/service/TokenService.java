package ru.iteco.at.center.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface TokenService {

    @NotNull
    String generateToken();

    boolean validateToken(@Nullable final String token);
}
