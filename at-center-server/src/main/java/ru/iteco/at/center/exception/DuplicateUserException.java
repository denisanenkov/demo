package ru.iteco.at.center.exception;

public class DuplicateUserException extends RuntimeException {
    public DuplicateUserException() {
        super("Login already exists.");
    }
}
