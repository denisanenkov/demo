package ru.iteco.at.center.model.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.iteco.at.center.enumerate.SystemCapacity;
import ru.iteco.at.center.model.dto.SystemDTO;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "app_system")
@NoArgsConstructor
@EqualsAndHashCode(of = "id", callSuper = true)
public class OperationSystem extends AbstractEntity implements Serializable {

    @Nullable
    private String name;

    @Nullable
    @Enumerated(EnumType.STRING)
    private SystemCapacity systemCapacity;

    @Nullable
    private String description;

    @Nullable
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "operationSystem", cascade = CascadeType.REMOVE)
    private List<JobInstance> jobInstanceList;

    @NotNull
    public OperationSystem(
            @Nullable final String name,
            @Nullable final SystemCapacity systemCapacity,
            @Nullable final List<JobInstance> jobInstanceList
    ) {
        this.name = name;
        this.systemCapacity = systemCapacity;
        this.jobInstanceList = jobInstanceList;
    }

    @NotNull
    public static OperationSystem toSystem(@NotNull final SystemDTO systemDTO) {
        @NotNull final OperationSystem operationSystem = new OperationSystem();
        operationSystem.setId(systemDTO.getId());
        operationSystem.setName(systemDTO.getName());
        operationSystem.setSystemCapacity(systemDTO.getSystemCapacity());
        return operationSystem;
    }

    @Override
    public boolean equals(@Nullable final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        @NotNull final OperationSystem that = (OperationSystem) o;

        if (!name.equals(that.name)) return false;
        if (systemCapacity != that.systemCapacity) return false;
        return jobInstanceList != null ? jobInstanceList.equals(that.jobInstanceList) : that.jobInstanceList == null;
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + systemCapacity.hashCode();
        result = 31 * result + (jobInstanceList != null ? jobInstanceList.hashCode() : 0);
        return result;
    }

}