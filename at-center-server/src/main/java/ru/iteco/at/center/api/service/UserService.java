package ru.iteco.at.center.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.iteco.at.center.model.entity.User;

import java.util.List;

@Service
@Transactional
public interface UserService {

    long count();

    void deleteAll();

    @NotNull
    List<User> findAll();

    @NotNull
    User findLoggedUser();

    @NotNull
    User create(User user);

    @NotNull
    User findById(String id);

    @NotNull
    User generateToken(User user);

    @NotNull
    User findByToken(String token);

    @Nullable
    User findByLogin(String login);

    void saveUser(@NotNull User user);

    String findIdByToken(String token);

    void deleteUser(@NotNull User user);

    void deleteUserById(@NotNull final String id);

    @NotNull
    List<User> findAllByLastName(@NotNull final String lastName);

    @NotNull
    List<User> findAllByFirstName(@NotNull final String firstName);

    @NotNull
    List<User> findAllByMiddleName(@NotNull final String secondName);

    @NotNull
    List<User> findAllByEmail(@NotNull String email);

    @NotNull
    List<User> findAllById(@NotNull final String id);

    boolean existsByToken(@NotNull String token);

    boolean existsByLogin(@NotNull String login);

}
