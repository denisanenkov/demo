package ru.iteco.at.center.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.iteco.at.center.api.repository.CategoryRepository;
import ru.iteco.at.center.api.service.CategoryService;
import ru.iteco.at.center.exception.CategoryNotFoundException;
import ru.iteco.at.center.model.entity.Category;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import static java.util.Comparator.naturalOrder;


@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    @Transactional
    public void create(@NotNull Category category) {
        if (existsById(category.getId())) return;
        categoryRepository.save(category);
    }

    @Transactional
    public void edit(@NotNull Category category) {
        categoryRepository.save(category);
    }

    @NotNull
    public Category findById(@NotNull final String id) {
        return categoryRepository.findById(id).orElseThrow(CategoryNotFoundException::new);
    }

    public boolean existsById(@NotNull final String id) {
        return categoryRepository.existsById(id);
    }

    @Transactional
    public void deleteById(@NotNull final String id) {
        categoryRepository.deleteById(id);
    }

    @Transactional
    public void delete(@NotNull final Category category) {
        categoryRepository.delete(category);
    }

    @Transactional
    public void deleteAll(@NotNull final Iterable<? extends Category> categories) {
        categoryRepository.deleteAll(categories);
    }

    @NotNull
    @Override
    public List<Category> findAll() {
        return categoryRepository.findAllByOrderByNameAsc().stream()
                                 .sorted(Comparator.nullsLast(Comparator
                                         .comparing(Category::getName, Comparator.nullsLast(naturalOrder()))))
                                 .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<Category> searchByName(@NotNull final String searchString) {
        return categoryRepository.findAllByNameContainingIgnoreCaseOrderByName(searchString).stream()
                .sorted(Comparator.nullsLast(Comparator
                        .comparing(Category::getName, Comparator.nullsLast(naturalOrder()))))
                .collect(Collectors.toList());
    }
    
}
