package ru.iteco.at.center.model.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.iteco.at.center.exception.EmptyEntityException;
import ru.iteco.at.center.model.dto.JobDTO;
import ru.iteco.at.center.model.dto.UserDTO;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "app_job")
@NoArgsConstructor
@EqualsAndHashCode(of = "id", callSuper = true)
public class Job extends AbstractEntity implements Serializable {

    @Nullable
    private String name;

    @NotNull
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "job", cascade = {CascadeType.PERSIST, CascadeType.REMOVE}, orphanRemoval = true)
    private List<JobInstance> jobInstanceList = new ArrayList<>();

    @NotNull
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "job", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE}, orphanRemoval = true)
    private List<CategoryJob> categoryJobList = new ArrayList<>();

    @Nullable
    private String buildNumber;

    @Nullable
    private String description;

}
