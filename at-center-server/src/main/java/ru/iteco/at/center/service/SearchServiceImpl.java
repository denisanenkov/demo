package ru.iteco.at.center.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import ru.iteco.at.center.api.annotation.SearchStringFieldMarker;
import ru.iteco.at.center.api.repository.UserRepository;
import ru.iteco.at.center.api.service.SearchService;
import ru.iteco.at.center.model.entity.User;

import javax.persistence.criteria.*;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SearchServiceImpl implements SearchService {
    @Autowired
    private UserRepository userRepository;
    @NotNull
    private final Map<String, String> userFieldMap = fillMap(User.class);

    @Override
    @NotNull
    public List<String> getUserFieldPrettyName() {
        @NotNull final List<String> prettyNameList = new ArrayList<>(userFieldMap.keySet());
        return prettyNameList;
    }

    /**
     * В качестве критериев ({@link Predicate}) для поиска метод использует значения, полученные из {@link #userFieldMap}
     * по ключам, переданным в массиве строк prettyNameUserFields.
     * Запрос формируется в зависимости от количества полей, по которым происходит поиск.
     * <p>Данный вид поиска, с использованием аннотации {@link SearchStringFieldMarker}? возможен, если поля в БД
     * мапятся по имени поля, без использования параметра name аннотации {@link javax.persistence.Column}</p>
     * @param prettyNameUserFields ключи для {@link #userFieldMap}
     * @param searchString строка поиска
     * @return заполненный List с пользователями
     */
    @Override
    @NotNull
    public List<User> searchUserByString(@NotNull final String[] prettyNameUserFields,
                                         @NotNull final String searchString) {
        @NotNull final List<String> userFieldsList = new ArrayList<>();
        for (@NotNull final String prettyName : prettyNameUserFields) {
            userFieldsList.add(userFieldMap.get(prettyName));
        }
        return userRepository.findAll((Specification<User>) (root, criteriaQuery, criteriaBuilder) -> {
            @NotNull final List<Predicate> predicates = new ArrayList<>();
            for (@NotNull final String userField : userFieldsList) {
                @NotNull final Expression<String> path = root.get(userField);
                @NotNull final Predicate predicate = criteriaBuilder.like(path, "%" + searchString + "%");
                predicates.add(predicate);
            }
            return criteriaBuilder.or(predicates.toArray(new Predicate[predicates.size()]));
        });
    }

    /**
     * Метод заполняет HashMap, где key это параметр "prettyname" аннотации {@link SearchStringFieldMarker},
     * а value это имя переменной.
     * @param clazz класс сущности, где аннотацией {@link SearchStringFieldMarker} помечены поля с типом String,
     *              необходимые для участия в поиске.
     * @return заполненную {@link HashMap}, если хотябы одно поле помечено аннотацией {@link SearchStringFieldMarker}
     */
    private Map<String, String> fillMap(@NotNull final Class clazz) {
        @NotNull final HashMap<String, String> fieldMap = new HashMap<>();
        @NotNull final Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(SearchStringFieldMarker.class)) {
                String prettyName = field.getDeclaredAnnotation(SearchStringFieldMarker.class).prettyName();
                String fieldName = field.getName();
                fieldMap.put(prettyName, fieldName);
            }
        }
        return fieldMap;
    }
}
