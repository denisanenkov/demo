package ru.iteco.at.center.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.iteco.at.center.api.service.UserService;
import ru.iteco.at.center.model.dto.UserDTO;
import ru.iteco.at.center.model.entity.User;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.*;

@Component
public class UserSearchUtil {

    @NotNull
    @PersistenceContext
    private EntityManager entityManager;

    @NotNull
    @SuppressWarnings("unchecked")
    public Set<UserDTO> findByParameterWithCriteria(@NotNull final String parameter) {
        @NotNull final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<User> criteriaQuery = criteriaBuilder.createQuery(User.class);
        @NotNull final Root<User> root = criteriaQuery.from(User.class);
        @NotNull final Predicate firstNameUserPredicate = criteriaBuilder.equal(root.get("firstName"), parameter);
        @NotNull final Predicate secondNameUserPredicate = criteriaBuilder.equal(root.get("middleName"), parameter);
        @NotNull final Predicate lastNameUserPredicate = criteriaBuilder.equal(root.get("lastName"), parameter);
        @NotNull final Predicate tokenUserPredicate = criteriaBuilder.equal(root.get("token"), parameter);
        @NotNull final Predicate emailPredicate = criteriaBuilder.equal(root.get("email"), parameter);
        @NotNull final Predicate criteriaOrPredicate = criteriaBuilder.or
                (firstNameUserPredicate, secondNameUserPredicate, lastNameUserPredicate, tokenUserPredicate, emailPredicate);
        criteriaQuery.where(criteriaOrPredicate);
        @NotNull final TypedQuery<User> query = entityManager.createQuery(criteriaQuery);
        @NotNull final Set<User> userSet = new HashSet(query.getResultList());
        @NotNull Set<UserDTO> userDTOSet = new HashSet<>();
        for (@NotNull final User user : userSet) {
            userDTOSet.add(ConvertEntityToDto.toUserDTO(user));
        }
        return userDTOSet;
    }

    @NotNull
    @SuppressWarnings("unchecked")
    public Set<UserDTO> findByName(
            @NotNull final String firstName,
            @NotNull final String middleName,
            @NotNull final String lastName,
            @NotNull final UserService userService
    ) {
        @NotNull Set<UserDTO> intersect = new HashSet<>();
        @NotNull List<UserDTO> findByFirstName = Collections.EMPTY_LIST;
        @NotNull List<UserDTO> findByMiddleName = Collections.EMPTY_LIST;
        @NotNull List<UserDTO> findByLastName = Collections.EMPTY_LIST;
        if (!firstName.isEmpty()) {
            findByFirstName = ConvertEntityToDto.toUserDTOList(userService.findAllByFirstName(firstName));
            if (findByFirstName.isEmpty()) return Collections.EMPTY_SET;
            intersect = new HashSet<>(findByFirstName);
        }
        if (!middleName.isEmpty()) {
            findByMiddleName = ConvertEntityToDto.toUserDTOList(userService.findAllByMiddleName(middleName));
            if (findByMiddleName.isEmpty()) return Collections.EMPTY_SET;
            if (intersect.isEmpty() && !findByMiddleName.isEmpty()) intersect = new HashSet<>(findByMiddleName);
            @NotNull List<UserDTO> withMiddleCompare = new ArrayList<>(intersect);
            intersect = intersect(withMiddleCompare, findByMiddleName);
        }
        if (!lastName.isEmpty()) {
            findByLastName = ConvertEntityToDto.toUserDTOList(userService.findAllByLastName(lastName));
            if (findByLastName.isEmpty()) return Collections.EMPTY_SET;
            if (intersect.isEmpty() && !findByLastName.isEmpty()) intersect = new HashSet<>(findByLastName);
            @NotNull List<UserDTO> withLastCompare = new ArrayList<>(intersect);
            intersect = intersect(withLastCompare, findByLastName);
        }
        return intersect;
    }

    @NotNull
    @SuppressWarnings("unchecked")
    public Set<UserDTO> intersect(
            @NotNull final List<UserDTO> userDTOSet1,
            @NotNull final List<UserDTO> userDTOSet2
    ) {
        @NotNull Set<UserDTO> userDTOIntersectSet = new HashSet<>();
        if (userDTOSet1.isEmpty() || userDTOSet2.isEmpty()) return Collections.EMPTY_SET;
        for (@NotNull final UserDTO userDTO1 : userDTOSet1) {
            for (@NotNull final UserDTO userDTO2 : userDTOSet2) {
                if (userDTO1.getId().equals(userDTO2.getId())) {
                    userDTOIntersectSet.add(userDTO1);
                }
            }
        }
        return userDTOIntersectSet;
    }

}
