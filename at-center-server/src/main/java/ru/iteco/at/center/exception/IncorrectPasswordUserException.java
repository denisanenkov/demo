package ru.iteco.at.center.exception;

public class IncorrectPasswordUserException extends RuntimeException {
    public IncorrectPasswordUserException() {
        super("Incorrect password.");
    }
}
