package ru.iteco.at.center.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.iteco.at.center.api.repository.MetrikValueRepository;
import ru.iteco.at.center.api.service.MetrikValueService;
import ru.iteco.at.center.exception.MetrikNotFoundException;
import ru.iteco.at.center.model.entity.MetrikValue;

import java.util.List;

@Service
public class MetrikValueServiceImpl implements MetrikValueService {

    @Autowired
    private MetrikValueRepository metrikValueRepository;

    @NotNull
    @Transactional
    public MetrikValue save(@NotNull final MetrikValue metrikValue) {
        return metrikValueRepository.save(metrikValue);
    }

    @NotNull
    @Override
    public MetrikValue findById(@NotNull final String id) {
        return metrikValueRepository.findById(id).orElseThrow(MetrikNotFoundException::new);
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return metrikValueRepository.existsById(id);
    }

    @Override
    @Transactional
    public void deleteById(@NotNull final String id) {
        metrikValueRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void delete(@NotNull final MetrikValue metrikValue) {
        metrikValueRepository.delete(metrikValue);
    }

    @Override
    @NotNull
    public Iterable<MetrikValue> findAllByMetrikId(@NotNull final String metrikId) {
        return metrikValueRepository.findAllByMetrikId(metrikId);
    }

    @Override
    public boolean existsByMetrikId(@NotNull final String metrikId) {
        return metrikValueRepository.existsByMetrikId(metrikId);
    }

    @Override
    @Transactional
    public void deleteAllByMetrikId(@NotNull final String metrikId) {
        metrikValueRepository.deleteAllByMetrikId(metrikId);
    }

    @NotNull
    @Override
    public List<MetrikValue> findAllByMetrikIdOrderByTimeStamp(@NotNull final String metrikId) {
        return metrikValueRepository.findAllByMetrikIdOrderByTimeStamp(metrikId);
    }
}
