package ru.iteco.at.center.api.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.iteco.at.center.model.entity.User;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, String>, JpaSpecificationExecutor<User> {

    User findByLogin(@NotNull final String login);

    void deleteUserById(@NotNull final String id);

    boolean existsByToken(@NotNull final String token);

    boolean existsByLogin(@NotNull final String login);

    @NotNull
    Optional<User> findOptionalByToken(@NotNull final String token);

    @Query("SELECT u.id FROM User u WHERE u.token = ?1")
    String findIdByToken(String token);

    List<User> findAllByOrderByLogin();

    List<User> findAllByFirstName(@NotNull final String firstName);

    List<User> findAllByMiddleName(@NotNull final String secondName);

    List<User> findAllByLastName(@NotNull final String lastName);

    List<User> findAllByEmail(@NotNull final String email);

    List<User> findAllById(@NotNull final String id);

}
