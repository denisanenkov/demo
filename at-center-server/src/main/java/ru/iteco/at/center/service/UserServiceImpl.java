package ru.iteco.at.center.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.iteco.at.center.api.repository.UserRepository;
import ru.iteco.at.center.api.service.AuthenticationFacade;
import ru.iteco.at.center.api.service.TokenService;
import ru.iteco.at.center.api.service.UserService;
import ru.iteco.at.center.exception.UserNotFoundException;
import ru.iteco.at.center.exception.UsernameNotFoundException;
import ru.iteco.at.center.model.entity.User;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Comparator.naturalOrder;

@Service
@Primary
@Transactional
public class UserServiceImpl implements UserService, UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthenticationFacade authenticationFacade;

    @Autowired
    private TokenService tokenService;

    @Override
    public UserDetails loadUserByUsername(String username) {
        final User user = userRepository.findByLogin(username);
        if (user == null) throw new UsernameNotFoundException(username);
        return user;
    }

    @NotNull
    public User findLoggedUser() {
        final Authentication authentication = authenticationFacade.getAuthentication();
        if (authentication instanceof AnonymousAuthenticationToken) throw new UserNotFoundException();
        final String currentUserName = authentication.getName();
        return userRepository.findByLogin(currentUserName);
    }

    @NotNull
    @Override
    public User findById(@NotNull final String id) {
        return userRepository.findById(id).orElseThrow(UserNotFoundException::new);
    }

    @Nullable
    public User findByLogin(@NotNull final String login) {
        return userRepository.findByLogin(login);
    }

    @NotNull
    @Override
    public User create(@NotNull final User user) {
        return userRepository.save(user);
    }

    @NotNull
    @Override
    public User generateToken(@NotNull final User user) {
        final String token = tokenService.generateToken();
        user.setToken(token);
        return userRepository.save(user);
    }

    @NotNull
    @Override
    public User findByToken(@NotNull final String token) {
        return userRepository.findOptionalByToken(token).orElseThrow(UserNotFoundException::new);
    }

    @Override
    public String findIdByToken(@NotNull final String token) {
        return userRepository.findIdByToken(token);
    }

    @NotNull
    @Override
    public List<User> findAll(){
        return userRepository.findAllByOrderByLogin().stream()
                .sorted(Comparator.nullsLast(Comparator
                        .comparing(User::getLogin, Comparator.nullsLast(naturalOrder()))))
                .collect(Collectors.toList());
    }

    @Override
    public void deleteUser(@NotNull final User user) {
        userRepository.delete(user);
    }

    @Override
    public void saveUser(@NotNull final User user) {
        userRepository.save(user);
    }

    @Override
    public void deleteUserById(@NotNull final String id) {
        userRepository.deleteUserById(id);
    }

    @Override
    public long count() { return userRepository.count(); }

    @Override
    public void deleteAll() { userRepository.deleteAll(); }

    @NotNull
    @Override
    public List<User> findAllByFirstName(@NotNull final String firstName) {
        return userRepository.findAllByFirstName(firstName);
    }

    @NotNull
    @Override
    public List<User> findAllByMiddleName(@NotNull final String secondName) {
        return userRepository.findAllByMiddleName(secondName);
    }

    @NotNull
    @Override
    public List<User> findAllByLastName(@NotNull final String lastName) {
        return userRepository.findAllByLastName(lastName);
    }

    @NotNull
    @Override
    public List<User> findAllByEmail(@NotNull String email) {
        return userRepository.findAllByEmail(email);
    }

    @NotNull
    @Override
    public List<User> findAllById(@NotNull final String id) {
        return userRepository.findAllById(id);
    }

    @Override
    public boolean existsByToken(@NotNull String token) {
        return userRepository.existsByToken(token);
    }

    @Override
    public boolean existsByLogin(@NotNull String login) {
        return userRepository.existsByLogin(login);
    }

}
