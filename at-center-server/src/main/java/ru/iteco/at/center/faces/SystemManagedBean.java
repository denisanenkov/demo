package ru.iteco.at.center.faces;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import ru.iteco.at.center.api.service.JobInstanceService;
import ru.iteco.at.center.api.service.SystemService;
import ru.iteco.at.center.api.service.UserService;
import ru.iteco.at.center.model.entity.JobInstance;
import ru.iteco.at.center.model.entity.OperationSystem;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Getter
@Setter
@SessionScoped
@ManagedBean(name = "systemBean")
public class SystemManagedBean extends SpringBeanAutowiringSupport {

    @Autowired
    private SystemService systemService;

    @Autowired
    private UserService userService;

    @Autowired
    private JobInstanceService jobInstanceService;

    @NotNull
    private OperationSystem operationSystem = new OperationSystem();

    @NotNull
    private List<OperationSystem> list = new ArrayList<>();

    private String searchString;

    private Collection<JobInstance> jobInstancesList;

    @NotNull
    public String viewJobsBySystem(@NotNull final OperationSystem operationSystem) {
        this.operationSystem = operationSystem;
        jobInstancesList = jobInstanceService.findAllByOperationSystemId(operationSystem.getId());
        return "pretty:viewSystem";
    }

    public void prepareJobsBySystem() {
        final String id = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("id");
        if (jobInstancesList != null && operationSystem.getId().equals(id)) return;
        jobInstancesList = jobInstanceService.findAllByOperationSystemId(id);
    }

    public void deleteSystem(@Nullable OperationSystem operationSystem) {
        if (operationSystem == null) return;
        systemService.deleteById(operationSystem.getId());
    }

    public String editSystem(@NotNull final OperationSystem operationSystem) {
        this.operationSystem = operationSystem;
        return "pretty:editSystem";
    }

    public void viewEditSystem() {
        final String id = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("id");
        this.operationSystem = systemService.findById(id);
    }

    public String viewInfoSystem(@NotNull final OperationSystem operationSystem) {
        this.operationSystem = operationSystem;
        return "pretty:infoSystem";
    }

    @NotNull
    public String saveSystem() {
        systemService.save(operationSystem);
        return list();
    }

    public String list() {
        searchString = "";
        list = (List<OperationSystem>) systemService.findAll();
        return "pretty:systemList";
    }

    @NotNull
    public List<OperationSystem> search() {
        list = (List<OperationSystem>) systemService.searchByName(searchString);
        return list;
    }

    @NotNull
    public List<OperationSystem> resetSearch() {
        searchString = "";
        return (List<OperationSystem>) systemService.findAll();
    }

    public void setSystemList(@NotNull final List<OperationSystem> systemList) {
        this.list = systemList;
    }

}
