package ru.iteco.at.center.api.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.iteco.at.center.enumerate.TypeMetrik;
import ru.iteco.at.center.model.entity.Metrik;

import java.util.Optional;

@Repository
public interface MetrikRepository extends JpaRepository<Metrik, String> {

    @NotNull
    Iterable<Metrik> findAllByJobInstanceId(@NotNull final String jobInstanceId);

    void deleteAllByJobInstanceId(@NotNull final String jobInstanceId);

    boolean existsByJobInstanceId(@NotNull final String jobInstanceId);
    @NotNull
    Optional<Metrik> findByJobInstanceIdAndType(@NotNull final String id, @NotNull final TypeMetrik typeMetrik);
}
