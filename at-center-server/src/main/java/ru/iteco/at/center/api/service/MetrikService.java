package ru.iteco.at.center.api.service;

import org.jetbrains.annotations.NotNull;
import ru.iteco.at.center.enumerate.TypeMetrik;
import ru.iteco.at.center.model.entity.Metrik;

import java.util.List;

public interface MetrikService {
    @NotNull
    Metrik save(@NotNull Metrik metrik);

    @NotNull
    List<Metrik> saveAll(@NotNull Iterable<Metrik> iterable);

    @NotNull
    Metrik findById(@NotNull String id);

    @NotNull
    Iterable<Metrik> findAllByJobInstanceId(@NotNull String jobInstanceId);

    boolean existsById(@NotNull String id);

    void delete(@NotNull Metrik metrik);

    void deleteById(@NotNull String id);

    void deleteAll(@NotNull Iterable<Metrik> entities);

    void deleteAllByJobInstanceId(@NotNull String jobInstanceId);

    boolean existsByJobInstanceId(@NotNull String jobInstanceId);

    @NotNull
    Metrik findByJobInstanceIdAndType(@NotNull String id, @NotNull TypeMetrik typeMetrik);
}
