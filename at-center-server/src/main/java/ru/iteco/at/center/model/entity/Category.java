package ru.iteco.at.center.model.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "app_category")
@EqualsAndHashCode(of = "id", callSuper = true)
public class Category extends AbstractEntity implements Serializable {

    @Nullable
    private String name;

    @Nullable
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "category", cascade = CascadeType.REMOVE)
    private List<CategoryJob> categoryJobList;

    @Nullable
    private String description;
}
