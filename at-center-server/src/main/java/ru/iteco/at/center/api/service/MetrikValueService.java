package ru.iteco.at.center.api.service;

import org.jetbrains.annotations.NotNull;
import ru.iteco.at.center.model.entity.MetrikValue;

import java.util.List;

public interface MetrikValueService {
    @NotNull
    MetrikValue save(@NotNull final MetrikValue metrikValue);

    @NotNull
    MetrikValue findById(@NotNull final String id);

    boolean existsById(@NotNull final String id);

    void deleteById(@NotNull final String id);

    void delete(@NotNull final MetrikValue metrikValue);

    @NotNull
    Iterable<MetrikValue> findAllByMetrikId(@NotNull final String metrikId);

    public boolean existsByMetrikId(@NotNull final String metrikId);

    void deleteAllByMetrikId(@NotNull final String metrikId);

    @NotNull
    List<MetrikValue> findAllByMetrikIdOrderByTimeStamp(@NotNull final String metrikId);
}
