package ru.iteco.at.center.faces;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import ru.iteco.at.center.api.service.SearchService;
import ru.iteco.at.center.api.service.UserService;
import ru.iteco.at.center.model.entity.User;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.util.List;

@Getter
@Setter
@SessionScoped
@ManagedBean(name = "searchBean")
public class SearchManagedBean extends SpringBeanAutowiringSupport {

    @Autowired
    private SearchService searchService;

    @Autowired
    private UserService userService;

    @NotNull
    private List<String> userFieldPrettyName = searchService.getUserFieldPrettyName();

    private String searchString;

    private String[] selectedFields;

    @NotNull
    public List<User> searchUserByString() {
        return searchService.searchUserByString(selectedFields, searchString);
    }

    @NotNull
    public List<User> resetSearch() {
        searchString = "";
        selectedFields = new String[0];
        return userService.findAll();
    }

}
