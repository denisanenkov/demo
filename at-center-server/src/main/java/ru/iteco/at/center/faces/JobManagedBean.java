package ru.iteco.at.center.faces;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.primefaces.model.charts.line.LineChartModel;
import org.primefaces.model.charts.pie.PieChartModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import ru.iteco.at.center.api.service.*;
import ru.iteco.at.center.exception.CategoryNotFoundException;
import ru.iteco.at.center.model.entity.*;
import ru.iteco.at.center.util.GraphsBuilderUtil;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import java.util.*;

@Getter
@Setter
@SessionScoped
@ManagedBean(name = "jobBean")
public class JobManagedBean extends SpringBeanAutowiringSupport {

    private static final String PRETTY_VIEW_JOB = "pretty:viewJob";

    private static final String PRETTY_CREATE_JOB = "pretty:createJob";

    private static final String PRETTY_EDIT_JOB = "pretty:editJob";

    private static final String PRETTY_JOB_LIST = "pretty:jobList";

    private static final String PRETTY_VIEW_JOB_PROFILE = "pretty:viewJobProfile";

    @Autowired
    private JobService jobService;

    @Autowired
    private UserService userService;

    @Autowired
    private JobInstanceService jobInstanceService;

    @Autowired
    private CategoryJobService categoryJobService;

    @Autowired
    private SystemService systemService;

    @NotNull
    private Job job = new Job();

    @NotNull
    private LineChartModel lineModel = new LineChartModel();

    @NotNull
    private Collection<JobInstance> jobInstancesList = new ArrayList<>();

    @NotNull
    private List<Job> list = new ArrayList<>();

    private String searchString;

    @NotNull
    public String viewJobsByJob(@NotNull final Job job) {
        jobInstancesList = jobInstanceService.findAllByJobId(job.getId());
        return PRETTY_VIEW_JOB;
    }

    @NotNull
    public String viewJobProfile(@NotNull final Job job) {
        this.job = job;
        return PRETTY_VIEW_JOB_PROFILE;
    }

    @NotNull
    public String viewJobsByUser(@NotNull final User user) {
        jobInstancesList = jobInstanceService.findAllByUserId(user.getId());
        return PRETTY_VIEW_JOB;
    }

    @NotNull
    public String createJob() {
        job = new Job();
        return PRETTY_CREATE_JOB;
    }

    @NotNull
    public String editJob(@NotNull final Job job) {
        this.job = job;
        return PRETTY_EDIT_JOB;
    }

    public void viewEditJob() {
        final String id = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("id");
        this.job = jobService.findById(id);
    }

    @NotNull
    public String saveJob() {
        jobService.save(job);
        return PRETTY_JOB_LIST;
    }

    @NotNull
    public String list() {
        searchString = "";
        list = jobService.findAll();
        return PRETTY_JOB_LIST;
    }

    public boolean jobInstanceExists() {
        return jobInstanceService.existsByJobId(job.getId());
    }

    @NotNull
    private Iterable<JobInstance> jobInstanceList() {
        return jobInstanceService.findAllByJobId(job.getId());
    }

    public Iterable<Category> categoriesByJobId(@NotNull final String jobId) {
        return categoryJobService.findAllCategoriesByJobId(jobId);
    }

    public String deleteJob(@NotNull final Job job) {
        jobService.delete(job);
        return PRETTY_JOB_LIST;
    }

    @NotNull
    public PieChartModel getCorrelationSystemPieModel() {
        @NotNull final List<Number> values = new ArrayList<>();
        @NotNull final List<Object> labels = new ArrayList<>();
        @NotNull final Iterable<OperationSystem> operationSystems = systemService.findAll();
        @NotNull final Map<String, Integer> systemCountMap = systemMapByCurrentJob(jobInstancesList, operationSystems);
        for (@NotNull Map.Entry<String, Integer> entry : systemCountMap.entrySet()) {
            values.add(entry.getValue());
            labels.add(entry.getKey());
        }
        @NotNull final PieChartModel systemPieModel = GraphsBuilderUtil.buildPieGraphModel(values, labels);
        return systemPieModel;
    }

    @NotNull
    private Map<String, Integer> systemMapByCurrentJob(@NotNull final Iterable<JobInstance> instances,
                                                       @NotNull final Iterable<OperationSystem> systems) {
        @NotNull final Map<String, Integer> systemNameCountMap = new TreeMap<>();
        for (@NotNull final OperationSystem system : systems) {
            @NotNull String systemName = "";
            @NotNull Integer count = 0;
            for (JobInstance jobInstance : instances) {
                if (jobInstance.getOperationSystem() != null && system.equals(jobInstance.getOperationSystem()))
                    count++;
                if (system.getSystemCapacity() != null) {
                    systemName = system.getName() + " " + system.getSystemCapacity().getLabel();
                }
            }
            if (count > 0) {
                systemNameCountMap.put(systemName, count);
            }
        }
        return systemNameCountMap;
    }

    public void updateJobs() {
        if (!FacesContext.getCurrentInstance().isPostback())
            list = jobService.findAll();
    }

    public void updateJobsFor(@Nullable final String categoryId) {
        if (categoryId == null || "".equals(categoryId)) throw new CategoryNotFoundException();
        final boolean isNotPostback = !FacesContext.getCurrentInstance().isPostback();
        if (isNotPostback) list = jobService.findAll();
        else return;
        @Nullable final List<Job> categoryJobList = this.categoryJobService.findAllJobByCategoryId(categoryId);
        if (categoryJobList == null) return;
        categoryJobList.forEach(this::remove);
    }

    @NotNull
    public List<Job> search() {
        list = (List<Job>) jobService.findByName(searchString);
        return list;
    }

    @NotNull
    public List<Job> resetSearch() {
        searchString = "";
        return jobService.findAll();
    }

    public void setJobList(@NotNull final List<Job> jobList) {
        this.list = jobList;
    }

    private void remove(@NotNull final Job removeJob) {
        list.removeIf(job -> Objects.equals(job.getId(), removeJob.getId()));
    }

}