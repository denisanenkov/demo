package ru.iteco.at.center.api.service;

import org.jetbrains.annotations.NotNull;
import ru.iteco.at.center.model.dto.JobInstancePlainDTO;
import ru.iteco.at.center.model.entity.JobInstance;

import java.util.Collection;

public interface JobInstanceService {

    @NotNull
    Collection<JobInstance> findAllByJobId(@NotNull String jobId);

    @NotNull
    Collection<JobInstance> findAllByOperationSystemId(@NotNull String systemId);

    @NotNull
    Collection<JobInstance> findAllByUserId(@NotNull String userId);

    @NotNull
    JobInstance save(@NotNull JobInstance job);

    @NotNull
    JobInstance save(@NotNull JobInstancePlainDTO job);

    @NotNull
    JobInstance findById(@NotNull String id);

    @NotNull
    JobInstance findByIdAndUserId(@NotNull String id, @NotNull String jobId);

    boolean existsById(@NotNull String id);

    boolean existsByJobId(@NotNull String jobId);

    void deleteById(@NotNull String id);

    void delete(@NotNull JobInstance jobInstance);

    @NotNull
    Collection<JobInstance> findAll();

    void deleteAll();

}
