package ru.iteco.at.center.api.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.iteco.at.center.model.dto.JobPlainDTO;
import ru.iteco.at.center.model.entity.Job;

import java.util.List;

@Repository
public interface JobPlainDtoRepository extends JpaRepository<JobPlainDTO, String> {

    List<JobPlainDTO> findAllByOrderByNameAsc();

    List<JobPlainDTO> findAllByNameContainingIgnoreCaseOrderByName(@NotNull final String searchString);

    List<JobPlainDTO> findAllByName(@NotNull final String name);

}
