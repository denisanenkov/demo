package ru.iteco.at.center.api.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.iteco.at.center.model.dto.JobPlainDTO;

import java.util.List;

public interface IJobRestEndpoint {

    @PostMapping(value = "/job/dto")
    void saveJob(
            @RequestBody @Nullable JobPlainDTO jobPlainDTO
    );

    @Nullable
    @GetMapping("/job/find/id/{id}")
    JobPlainDTO findJobById(
            @PathVariable(value = "id") String id
    );

    @NotNull
    @PostMapping("/job/find/name")
    List<JobPlainDTO> findJobByName(@RequestBody JobPlainDTO jobPlainDTO);

    @GetMapping(value = "/job/exists/id/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    boolean existJobById(
            @PathVariable(name = "id") String id
    );

    @DeleteMapping(value = "/job/{id}")
    void deleteJobById(
            @PathVariable(name = "id") @Nullable String id
    );

    @NotNull
    @GetMapping(value = "/jobs", produces = MediaType.APPLICATION_JSON_VALUE)
    List<JobPlainDTO> findAllJobs();

    @DeleteMapping(value = "/jobs")
    void deleteAllJobs();

}
