package ru.iteco.at.center.faces;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.primefaces.model.charts.line.LineChartModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import ru.iteco.at.center.api.service.MetrikService;
import ru.iteco.at.center.api.service.MetrikValueService;
import ru.iteco.at.center.enumerate.TypeMetrik;
import ru.iteco.at.center.model.entity.JobInstance;
import ru.iteco.at.center.model.entity.Metrik;
import ru.iteco.at.center.model.entity.MetrikValue;
import ru.iteco.at.center.service.JobInstanceServiceImpl;
import ru.iteco.at.center.util.GraphsBuilderUtil;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import java.util.*;

@Getter
@Setter
@SessionScoped
@ManagedBean(name = "jobInstanceBean")
public class JobInstanceManagedBean extends SpringBeanAutowiringSupport {

    private static final String FORMAT_STRING = "Graph dependency between %s and %s metrics";

    @Autowired
    private JobInstanceServiceImpl jobInstanceService;

    @Autowired
    private MetrikService metrikService;

    @Autowired
    private MetrikValueService metrikValueService;

    @NotNull
    private JobInstance jobInstance = new JobInstance();

    private String fromPage;

    private String selectedOption;

    private int numberPointsOnGraphic = 1; // 1 - all points

    private String baseNumberPoints = "All";

    private String baseLeftBound = "Min";

    private String baseRightBound = "Max";

    private int leftBoundMsOnGraphic = 0;

    private long rightBoundMsOnGraphic = 9_999_999_999_999L;

    private static Map<String, String> numberPointsMap;

    private static Map<String, String> numberMsBoundsMap;

    private String localeCode = "en"; //default value

    static {
        numberPointsMap = new LinkedHashMap<String, String>();
        numberPointsMap.put("All", "1");
        numberPointsMap.put("100", "100");
        numberPointsMap.put("50", "50");
        numberPointsMap.put("20", "20");
        numberPointsMap.put("10", "10");
    }

    static {
        numberMsBoundsMap = new LinkedHashMap<>();
        numberMsBoundsMap.put("Max        ms.", "10000000");
        numberMsBoundsMap.put("1.500.000  ms.", "1500000");
        numberMsBoundsMap.put("1.200.000  ms.", "1200000");
        numberMsBoundsMap.put("1.000.000  ms.", "1000000");
        numberMsBoundsMap.put("800.000    ms.", "800000");
        numberMsBoundsMap.put("500.000    ms.", "500000");
        numberMsBoundsMap.put("300.000    ms.", "300000");
        numberMsBoundsMap.put("200.000    ms.", "200000");
        numberMsBoundsMap.put("100.000    ms.", "100000");
        numberMsBoundsMap.put("70.000     ms.", "70000");
        numberMsBoundsMap.put("50.000     ms.", "50000");
        numberMsBoundsMap.put("40.000     ms.", "40000");
        numberMsBoundsMap.put("30.000     ms.", "30000");
        numberMsBoundsMap.put("20.000     ms.", "20000");
        numberMsBoundsMap.put("10.000     ms.", "10000");
        numberMsBoundsMap.put("5.000      ms.", "5000");
        numberMsBoundsMap.put("1.000      ms.", "1000");
        numberMsBoundsMap.put("Min        ms.", "0");
    }

    public void changePointsOnGraphic(ValueChangeEvent e) {
        numberPointsOnGraphic = Integer.parseInt(e.getNewValue().toString());
        System.err.println("point1");
    }

    public void changePointsLeftValueOfRangeOnGraphic(ValueChangeEvent e) {
        leftBoundMsOnGraphic = Integer.parseInt(e.getNewValue().toString());
    }

    public void changePointsRightValueOfRangeOnGraphic(ValueChangeEvent e) {
        rightBoundMsOnGraphic = Integer.parseInt(e.getNewValue().toString());
    }

    public Map<String, String> getNumberPointsMap() {
        return numberPointsMap;
    }

    public int getLeftBoundMsOnGraphic() {
        return leftBoundMsOnGraphic;
    }

    public void setLeftBoundMsOnGraphic(int leftBoundMsOnGraphic) {
        this.leftBoundMsOnGraphic = leftBoundMsOnGraphic;
    }

    public long getRightBoundMsOnGraphic() {
        return rightBoundMsOnGraphic;
    }

    public void setRightBoundMsOnGraphic(int rightBoundMsOnGraphic) {
        this.rightBoundMsOnGraphic = rightBoundMsOnGraphic;
    }

    public String getBaseRightBound() {
        return baseRightBound;
    }

    public void setBaseRightBound(String baseRightBound) {
        this.baseRightBound = baseRightBound;
    }

    public Map<String, String> getNumberMsBoundsMap() {
        return numberMsBoundsMap;
    }

    public static void setNumberMsBoundsMap(Map<String, String> numberMsBoundsMap) {
        JobInstanceManagedBean.numberMsBoundsMap = numberMsBoundsMap;
    }

    @NotNull
    public String viewJobInstance(@NotNull final JobInstance jobInstance) {
        this.jobInstance = jobInstance;
        return "pretty:viewJobInstance";
    }

    public boolean metricExists() {
        return metrikService.existsByJobInstanceId(jobInstance.getId());
    }

    @NotNull
    public Iterable<Metrik> metricList() {
        return metrikService.findAllByJobInstanceId(jobInstance.getId());
    }

    public String deleteJobInstance(final JobInstance jobInstance) {
        jobInstanceService.delete(jobInstance);
        return "pretty:jobList";
    }

    private String getAxesLabel(@NotNull final Metrik metric) {
        @NotNull String result = "";
        if (metric.getType() != null) {
            result = metric.getName() + ", " + metric.getType().getMeasure();
        }
        return result;
    }

    private void updateAsAverage(
            @NotNull final List<MetrikValue> listMetricValueY,
            @NotNull final List<MetrikValue> listMetricValueX,
            @NotNull final List<Number> listObjectY
    ) {
        for (int i = 0; i < listMetricValueY.size(); i++) {
            @Nullable final String valueY = listMetricValueY.get(i).getValue();
            @Nullable final String valueX = listMetricValueX.get(i).getValue();

            if (valueY != null && valueX != null) {
                final long longValueY = Long.parseLong(valueY);
                final long longValueX = Long.parseLong(valueX);
                @NotNull final Double averageValue = (longValueX == 0) ? longValueY : longValueY * 1.0 / longValueX;
                listObjectY.add(i, averageValue);
            }
        }
    }

    @NotNull
    public LineChartModel getGraphLineModel(
            final TypeMetrik typeMetricY,
            final boolean isAverage,
            final TypeMetrik typeMetricX
    ) {
        @NotNull final List<Number> listObjectY = new ArrayList<>();
        @NotNull final List<Object> listObjectX = new ArrayList<>();
        @NotNull final Metrik yMetric = metrikService.findByJobInstanceIdAndType(jobInstance.getId(), typeMetricY);
        @NotNull final Metrik xMetric = metrikService.findByJobInstanceIdAndType(jobInstance.getId(), typeMetricX);

        @NotNull final List<MetrikValue> propertyListMetricValueX =
                metrikValueService.findAllByMetrikIdOrderByTimeStamp(xMetric.getId());

        @NotNull final List<MetrikValue> propertyListMetricValueY =
                metrikValueService.findAllByMetrikIdOrderByTimeStamp(yMetric.getId());

        @NotNull final List<MetrikValue> listMetricValueY = new ArrayList<>();

        @NotNull final List<MetrikValue> listMetricValueX = new ArrayList<>();

        int counter = -1;

        for (MetrikValue metrikValue : propertyListMetricValueX) {
            counter++;
            int metricValue = Integer.parseInt(metrikValue.getValue());
            if (metricValue < rightBoundMsOnGraphic && metricValue > leftBoundMsOnGraphic) {
                listMetricValueX.add(metrikValue);
                listMetricValueY.add(propertyListMetricValueY.get(counter));
            }
        }

        @NotNull String yAxesLabel = getAxesLabel(yMetric);
        @NotNull String xAxesLabel = getAxesLabel(xMetric);
        @NotNull final String graphLabel = String.format(FORMAT_STRING, yMetric.getName(), xMetric.getName());

        if (isAverage) {
            updateAsAverage(listMetricValueY, listMetricValueX, listObjectY);
            yAxesLabel = "Average number of objects per ms, amount";
        } else {
            listMetricValueY.forEach(metricValue -> {
                if (metricValue.getValue() != null) {
                    @NotNull final Long value = Long.parseLong(metricValue.getValue());
                    listObjectY.add(value);
                }
            });
        }

        listMetricValueX.forEach(metricValue -> {
            if (metricValue.getValue() != null) {
                listObjectX.add(metricValue.getValue());
            }
        });

        @NotNull final List<Number> listObjectLessY = new ArrayList<>();
        @NotNull final List<Object> listObjectLessX = new ArrayList<>();

        int period;
        if (numberPointsOnGraphic == 1) {
            period = 1;
        } else {
            period = listObjectY.size() / numberPointsOnGraphic;
        }

        int j = 0;
        for (int i = 0; i < listObjectY.size(); i++) {
            j++;
            if (j > period) {
                listObjectLessY.add(listObjectY.get(i));
                if (i + 1 < listMetricValueX.size()) listObjectLessX.add(listObjectX.get(i));
                j = 0;
            }
        }

        @NotNull final LineChartModel graphLineModel = GraphsBuilderUtil.buildLineGraphModel(
                listObjectLessY, listObjectLessX, yAxesLabel, xAxesLabel, graphLabel);
        return graphLineModel;
    }

    @NotNull
    public LineChartModel getModelForObjectAmountLoadRAMGraph() {
        return getGraphLineModel(TypeMetrik.CURRENT_AMOUNT, false, TypeMetrik.LOAD_AVERAGE);
    }

    @NotNull
    public LineChartModel getModelForObjectAmountTimeSpentGraph() {
        return getGraphLineModel(TypeMetrik.CURRENT_AMOUNT, false, TypeMetrik.TIME_INTERVALS);
    }

    @NotNull
    public LineChartModel getModelForLoadRAMTimeSpentGraph() {
        return getGraphLineModel(TypeMetrik.LOAD_AVERAGE, false, TypeMetrik.TIME_INTERVALS);
    }

    @NotNull
    public LineChartModel getModelForAverageObjectValueTimeSpent() {
        return getGraphLineModel(TypeMetrik.CURRENT_AMOUNT, true, TypeMetrik.TIME_INTERVALS);
    }

    /*
     * Метод принимает строку from, которая в дальнейшем позволит определить на какую страничку
     * возвращаться по нажатию на кнопку BACK в jobInstance-info(метод returnPreviousPage() ниже).
     */
    public String viewJobInstanceProfile(@NotNull final JobInstance jobInstance, @NotNull final String from) {
        this.jobInstance = jobInstance;
        this.fromPage = from;
        return "pretty:viewJobInstanceProfile";
    }

    public void viewJobInstanceProfile() {
        final String id = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("id");
//        if (this.jobInstance.getId().equals(id)) return;
        this.jobInstance = jobInstanceService.findById(id);
    }

    /*
     * Метод принимает решение какую страницу вернуть при нажатии на кнопку BACK в jobInstance-info.
     */
    public String returnPreviousPage() {
        if (this.fromPage == null) return "pretty:index";
        if ("viewSystem".equals(this.fromPage)) return "pretty:viewSystem";
        if ("viewJob".equals(this.fromPage)) return "pretty:viewJob";
        return "pretty:index";
    }

}
