package ru.iteco.at.center.api.service;

public interface IPropertyService {

    String getDatasourceURL();

    String getDatasourceDriver();

    String getDatasourceUser();

    String getDatasourcePassword();

}
