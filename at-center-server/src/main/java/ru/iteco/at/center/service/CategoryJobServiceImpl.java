package ru.iteco.at.center.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.iteco.at.center.api.repository.CategoryJobRepository;
import ru.iteco.at.center.api.service.CategoryJobService;
import ru.iteco.at.center.api.service.JobService;
import ru.iteco.at.center.model.entity.Category;
import ru.iteco.at.center.model.entity.CategoryJob;
import ru.iteco.at.center.model.entity.Job;

import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static java.util.Comparator.naturalOrder;

@Service
public class CategoryJobServiceImpl implements CategoryJobService {

    @Autowired
    private JobService jobService;
    @Autowired
    private CategoryJobRepository categoryJobRepository;

    @Nullable
    public Iterable<Category> findAllCategoriesByJobId(@NotNull final String jobId) {
        return StreamSupport
                .stream(categoryJobRepository.findByJobId(jobId).spliterator(), false)
                .map(CategoryJob::getCategory)
                .sorted(Comparator.nullsLast(Comparator
                        .comparing(Category::getName, Comparator.nullsLast(naturalOrder()))))
                .collect(Collectors.toList());
    }

    @Nullable
    public List<Job> findAllJobByCategoryId(@NotNull final String categoryId) {
        return StreamSupport
                .stream(categoryJobRepository.findByCategoryId(categoryId).spliterator(), false)
                .map(CategoryJob::getJob)
                .sorted(Comparator.nullsLast(Comparator
                        .comparing(Job::getName, Comparator.nullsLast(naturalOrder()))))
                .collect(Collectors.toList());
    }

    @Override
    public boolean existsByCategoryId(@NotNull final String id) {
        return categoryJobRepository.existsByCategoryId(id);
    }

    @Override
    public void addJob(@NotNull final CategoryJob job) {
        if (!jobService.existsById(job.getJob().getId())) {
          return;
        }
        categoryJobRepository.save(job);
    }

}
