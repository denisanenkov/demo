package ru.iteco.at.center.util;

import org.jetbrains.annotations.NotNull;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

@ApplicationScoped
@ManagedBean(name = "dateUtil")
public final class DateUtil {
    @NotNull
    private final SimpleDateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy");

    @NotNull
    private final SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm:ss");

    @NotNull
    public String getOnlyDateFromDate(@NotNull final Date date) {
        if (date == null)
            return "";
        return dateFormatter.format(date);
    }

    @NotNull
    public String getOnlyTimeFromDate(@NotNull final Date date) {
        if (date == null)
            return "";
        return timeFormatter.format(date);
    }

    @NotNull
    public String getDurationLikeTime(@NotNull final Date startDate, @NotNull final Date finishDate) {
        if (startDate == null || finishDate == null )
            return "";
        timeFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        @NotNull final String duration = timeFormatter.format(new Date(finishDate.getTime() - startDate.getTime()));
        timeFormatter.setTimeZone(TimeZone.getDefault());
        return duration;
    }

    @NotNull
    public String getDateAndTimeFromDate(@NotNull final Date date) {
        if (date == null)
            return "";
        return dateFormatter.format(date) + " " + timeFormatter.format(date);
    }
}

