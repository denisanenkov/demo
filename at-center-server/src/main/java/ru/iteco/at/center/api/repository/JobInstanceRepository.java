package ru.iteco.at.center.api.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.iteco.at.center.model.entity.JobInstance;

import java.util.Collection;
import java.util.Optional;

@Repository
public interface JobInstanceRepository extends JpaRepository<JobInstance, String> {

    @NotNull
    Collection<JobInstance> findAllByJobId(@NotNull String jobId);

    @NotNull
    Collection<JobInstance> findAllByOperationSystemId(@NotNull String systemId);

    @NotNull
    Collection<JobInstance> findAllByUserId(@NotNull String userId);

    Optional<JobInstance> findByIdAndJobId(@NotNull String id, @NotNull String jobId);

    boolean existsByJobId(@NotNull String jobId);
}
