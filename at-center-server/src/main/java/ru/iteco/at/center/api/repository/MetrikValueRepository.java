package ru.iteco.at.center.api.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.iteco.at.center.model.entity.MetrikValue;

import java.util.List;

@Repository
public interface MetrikValueRepository extends JpaRepository<MetrikValue, String> {

    @NotNull
    Iterable<MetrikValue> findAllByMetrikId(@NotNull final String metrikId);

    void deleteAllByMetrikId(@NotNull final String metrikId);

    boolean existsByMetrikId(@NotNull final String metrikId);

    @NotNull
    List<MetrikValue> findAllByMetrikIdOrderByTimeStamp(@NotNull final String metrikID);
}
