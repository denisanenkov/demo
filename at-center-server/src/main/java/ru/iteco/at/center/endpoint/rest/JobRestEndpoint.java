package ru.iteco.at.center.endpoint.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.iteco.at.center.api.rest.IJobRestEndpoint;
import ru.iteco.at.center.api.service.JobService;
import ru.iteco.at.center.model.dto.JobPlainDTO;
import ru.iteco.at.center.service.JobPlainDtoServiceImpl;

import java.util.List;

@RestController
public class JobRestEndpoint implements IJobRestEndpoint {

    @NotNull
    @Autowired
    private JobPlainDtoServiceImpl jobPlainDtoService;

    @NotNull
    @Autowired
    private JobService jobService;

    @Override
    @PostMapping(value = "/job/dto")
    public void saveJob(
            @RequestBody @Nullable JobPlainDTO jobPlainDTO
    ) {
        jobPlainDtoService.save(jobPlainDTO);
    }

    @Override
    @Nullable
    @GetMapping("/job/find/id/{id}")
    public JobPlainDTO findJobById(
            @PathVariable(value = "id") String id
    ) {
        return jobPlainDtoService.findById(id);
    }

    @Override
    @NotNull
    @PostMapping("/job/find/name")
    public List<JobPlainDTO> findJobByName(@RequestBody JobPlainDTO jobPlainDTO) {
        return jobPlainDtoService.findByName(jobPlainDTO.getName());
    }

    @Override
    @GetMapping(value = "/job/exists/id/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public boolean existJobById(
            @PathVariable(name = "id") String id
    ) {
        return jobPlainDtoService.existsById(id);
    }


    @Override
    @DeleteMapping(value = "/job/{id}")
    public void deleteJobById(
            @PathVariable(name = "id") @Nullable final String id
    ) {
        jobService.deleteById(id);
    }

    @Override
    @NotNull
    @GetMapping(value = "/jobs", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<JobPlainDTO> findAllJobs() {
        return jobPlainDtoService.findAll();
    }

    @Override
    @DeleteMapping(value = "/jobs")
    public void deleteAllJobs() {
        jobService.deleteAll();
    }

}
