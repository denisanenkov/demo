package ru.iteco.at.center.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.iteco.at.center.api.repository.UserRepository;
import ru.iteco.at.center.api.service.TokenService;

import java.util.UUID;

@Service
public class TokenServiceImpl implements TokenService {

    @Value("${token.secret.key}")
    private String secretKey;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserRepository userRepository;

    @NotNull
    @Override
    public String generateToken() {
        return passwordEncoder.encode(secretKey+ UUID.randomUUID().toString());
    }

    @Override
    public boolean validateToken(@Nullable String token) {
        if (token == null || token.isEmpty()) return false;
        else return userRepository.existsByToken(token);
    }
}
