package ru.iteco.at.center.security.jwt;

import org.jetbrains.annotations.NotNull;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import ru.iteco.at.center.model.entity.User;
import ru.iteco.at.center.enumerate.Role;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Implementation of Factory Method for class {@link JwtUser}.
 *
 */

public final class JwtUserFactory {

    public JwtUserFactory() {
    }

    @NotNull
    public static JwtUser create(@NotNull User user) {
        return new JwtUser(
                user.getId(),
                user.getUsername(),
                user.getFirstName(),
                user.getLastName(),
                user.getEmail(),
                user.getPassword(),
                mapToGrantedAuthorities(new ArrayList<>(user.getRoles())),
                user.isEnabled()
        );
    }

    @NotNull
    private static List<GrantedAuthority> mapToGrantedAuthorities(@NotNull final List<Role> userRoles) {
        return userRoles.stream()
                .map(role -> new SimpleGrantedAuthority(role.name()))
                .collect(Collectors.toList());
    }

}
