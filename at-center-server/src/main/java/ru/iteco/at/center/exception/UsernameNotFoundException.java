package ru.iteco.at.center.exception;

public class UsernameNotFoundException
        extends org.springframework.security.core.userdetails.UsernameNotFoundException {

    public UsernameNotFoundException(String username) {
        super("User with username: " + username + " not found");
    }

    public UsernameNotFoundException(String msg, Throwable t) {
        super(msg, t);
    }

}
