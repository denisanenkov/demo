package ru.iteco.at.center.endpoint.rest;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.beans.factory.annotation.Autowired;
import ru.iteco.at.center.api.rest.IUserRestEndpoint;
import ru.iteco.at.center.api.service.UserService;
import org.springframework.web.bind.annotation.*;
import ru.iteco.at.center.exception.UserNotFoundException;
import ru.iteco.at.center.util.ConvertEntityToDto;
import ru.iteco.at.center.util.UserSearchUtil;
import ru.iteco.at.center.model.entity.User;
import org.jetbrains.annotations.Nullable;
import org.springframework.http.MediaType;
import org.jetbrains.annotations.NotNull;
import ru.iteco.at.center.model.dto.UserDTO;
import lombok.SneakyThrows;

import java.util.List;
import java.util.Set;

@RestController
public class UserRestEndpoint implements IUserRestEndpoint {

    @NotNull
    @Autowired
    private UserService userService;

    @NotNull
    @Autowired
    private UserSearchUtil userSearchUtil;

    @NotNull
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    @Nullable
    @SneakyThrows
    @GetMapping(value = "/users", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<UserDTO> findAllUsers() {
        @NotNull List<UserDTO> userDTOList = ConvertEntityToDto.toUserDTOList(userService.findAll());
        return userDTOList;
    }

    @Override
    @Nullable
    @SneakyThrows
    @GetMapping(value = "/users/{parameter}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Set<UserDTO> findUsersByParameter(
            @PathVariable(name = "parameter") @NotNull final String parameter
    ) {
        return userSearchUtil.findByParameterWithCriteria(parameter);
    }

    @Override
    @Nullable
    @SneakyThrows
    @RequestMapping("/users/name")
    public Set<UserDTO> findUserByName(
            @RequestParam(value = "firstName", required = false, defaultValue = "") String firstName,
            @RequestParam(value = "middleName", required = false, defaultValue = "") String middleName,
            @RequestParam(value = "lastName", required = false, defaultValue = "") String lastName
    ) {
        return userSearchUtil.findByName(firstName, middleName, lastName, userService);
    }

    @Override
    @SneakyThrows
    @PostMapping(value = "/user")
    public void saveUser(
            @RequestBody @Nullable UserDTO user
    ) {
        @NotNull String encodedPassword = passwordEncoder.encode(user.getPassword());
        user.setPassword(encodedPassword);
        userService.saveUser(User.toUser(user));
    }

    @Override
    @SneakyThrows
    @PutMapping(value = "/user")
    public void updateUser(
            @RequestBody @Nullable final UserDTO user
    ) {
        if (userService.findById(user.getId()).getPassword() != user.getPassword()) {
            @NotNull String encodedPassword = passwordEncoder.encode(user.getPassword());
            user.setPassword(encodedPassword);
        }
        userService.saveUser(User.toUser(user));
    }

    @Override
    @SneakyThrows
    @DeleteMapping(value = "/users")
    public void deleteAllUsers() {
        userService.deleteAll();
    }

    @Override
    @SneakyThrows
    @DeleteMapping(value = "/user/{id}")
    public void deleteUserById(
            @PathVariable(name = "id") @Nullable final String id
    ) {
        userService.deleteUserById(id);
    }

    @Override
    @SneakyThrows
    @GetMapping(value = "/users/count")
    public long countUsers() {
        return userService.count();
    }

    @Override
    @SneakyThrows
    @GetMapping(value = "/users/exists/login/{login}")
    public boolean existsByLogin(
            @PathVariable(name = "login") @NotNull final String login
    ) {
        return userService.existsByLogin(login);
    }

}
