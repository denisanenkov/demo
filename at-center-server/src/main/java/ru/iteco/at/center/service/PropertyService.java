package ru.iteco.at.center.service;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.iteco.at.center.api.service.IPropertyService;

/**
 * <p>Методы: <b>getDatasourceURL, getDatasourceDriver, getDatasourceUser, getDatasourcePassword</b>
 * возвращают значения для соединнения с базой данных в следующем приоритете:</p>
 * <ol>
 * <li>Системные переменные, переданные в JVM при запуске приложения;</li>
 * <li>Переменные операционной системы;</li>
 * <li>Переменные из application.property файла.</li>
 * </ol>
 */

@Service
@PropertySource("classpath:application.properties")
public class PropertyService implements IPropertyService {

    @Value("${datasource.url}")
    private String propertyURL;

    @Value("${datasource.driver}")
    private String propertyDriver;

    @Value("${datasource.user}")
    private String propertyUser;

    @Value("${datasource.password}")
    private String propertyPassword;

    @Value("#{environment['ATCENTER_DB_URL']}")
    private String environmentURL;

    @Value("#{environment['ATCENTER_DB_DRIVER']}")
    private String environmentDriver;

    @Value("#{environment['ATCENTER_DB_USER']}")
    private String environmentUser;

    @Value("#{environment['ATCENTER_DB_PASSWORD']}")
    private String environmentPassword;

    @Override
    public String getDatasourceURL() {
        @Nullable final String systemURL = System.getProperty("datasource.url");
        if (systemURL != null) return systemURL;
        if (environmentURL != null) return environmentURL;
        return propertyURL;
    }

    @Override
    public String getDatasourceDriver() {
        @Nullable final String systemDriver = System.getProperty("datasource.driver");
        if (systemDriver != null) return systemDriver;
        if (environmentDriver != null) return environmentDriver;
        return propertyDriver;
    }

    @Override
    public String getDatasourceUser() {
        @Nullable final String systemUser = System.getProperty("datasource.user");
        if (systemUser != null) return systemUser;
        if (environmentUser != null) return environmentUser;
        return propertyUser;
    }

    @Override
    public String getDatasourcePassword() {
        @Nullable final String systemPassword = System.getProperty("datasource.password");
        if (systemPassword != null) return systemPassword;
        if (environmentPassword != null) return environmentPassword;
        return propertyPassword;
    }

}
