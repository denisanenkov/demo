package ru.iteco.at.center.api.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.iteco.at.center.model.entity.CategoryJob;

@Repository
public interface CategoryJobRepository extends JpaRepository<CategoryJob, String> {

    @Query("SELECT p from CategoryJob p where p.job.id = :jobId order by p.category.name")
    Iterable<CategoryJob> findByJobId(@NotNull @Param("jobId") String jobId);

    @Query("SELECT p from CategoryJob p where p.category.id = :categoryId order by p.job.name")
    Iterable<CategoryJob> findByCategoryId(@NotNull @Param("categoryId") String categoryId);

    boolean existsByCategoryId(@NotNull String id);
}
