package ru.iteco.at.center.faces;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import ru.iteco.at.center.api.service.UserService;
import ru.iteco.at.center.enumerate.Role;
import ru.iteco.at.center.exception.DuplicateUserException;
import ru.iteco.at.center.exception.IncorrectPasswordUserException;
import ru.iteco.at.center.model.entity.User;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import java.util.*;

@Getter
@Setter
@SessionScoped
@ManagedBean(name = "userBean")
public class UserManagedBean extends SpringBeanAutowiringSupport {

    private static final String PRETTY_PROFILE = "pretty:profileUser";
    private static final String PRETTY_CHANGE_PASSWORD = "pretty:changePasswordUser";
    private static final String PRETTY_LOGIN = "pretty:loginUser";
    private static final String PRETTY_LOGOUT = "pretty:logoutUser";
    private static final String PRETTY_USER_MANAGEMENT = "pretty:userManagement";
    private static final String PRETTY_HOME = "pretty:index";

    @Autowired
    private UserService userService;

    @NotNull
    private User user = new User();

    @NotNull
    private User registryUser = new User();

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Nullable
    private String changedPassword;

    @NotNull
    private List<User> list = new ArrayList<>();

    //Не баг, все методы нужны, Exception скорей всего тоже
    public void validateLogin(
            @NotNull final FacesContext context,
            @NotNull final UIComponent component,
            @NotNull final Object value
    ) throws ValidatorException {
        @NotNull final String login = String.valueOf(value);
        boolean isCurrentLogin = !login.equals(userService.findLoggedUser().getLogin());
        boolean isDuplicateLogin = userService.findByLogin(login) != null;
        if (isCurrentLogin && isDuplicateLogin) {
            @NotNull final FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_WARN,
                    new DuplicateUserException().getMessage(), null);
            throw new ValidatorException(facesMessage);
        }
    }

    //Не баг, все методы нужны, Exception скорей всего тоже
    public void validateRegistrationLogin(
            @NotNull final FacesContext context,
            @NotNull final UIComponent component,
            @NotNull final Object value
    ) throws ValidatorException {
        @NotNull final String login = String.valueOf(value);
        boolean isDuplicateLogin = userService.findByLogin(login) != null;
        if (isDuplicateLogin) {
            @NotNull final FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_WARN,
                    new DuplicateUserException().getMessage(), null);
            throw new ValidatorException(facesMessage);
        }
    }

    //Не баг, все методы нужны, Exception скорей всего тоже
    public void validateCurrentPassword(
            @NotNull final FacesContext context,
            @NotNull final UIComponent component,
            @NotNull final Object value
    ) throws ValidatorException {
        @NotNull final String password = String.valueOf(value);
        if (!passwordEncoder.matches(password, user.getPassword())) {
            @NotNull final FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    new IncorrectPasswordUserException().getMessage(), null);
            throw new ValidatorException(facesMessage);
        }
    }

    @NotNull
    public String saveUserProfile() {
        boolean isChangedLogin = !Objects.equals(userService.findLoggedUser().getLogin(), user.getLogin());
        userService.create(user);
        if (isChangedLogin) return PRETTY_LOGOUT;
        return PRETTY_PROFILE;
    }

    @NotNull
    public String changePassword() {
        user.setPassword(passwordEncoder.encode(changedPassword));
        userService.create(user);
        return PRETTY_PROFILE;
    }

    @NotNull
    public String createUser() {
        @Nullable final String password = registryUser.getPassword();
        registryUser.setPassword(passwordEncoder.encode(password));
        registryUser.getRoles().add(Role.USER);
        userService.create(registryUser);
        this.list.add(registryUser);
        registryUser = new User();
        if (user.getRoles().contains(Role.ADMIN)) return PRETTY_USER_MANAGEMENT;
        return PRETTY_LOGIN;
    }

    @NotNull
    public String viewUser() {
        this.user = getCurrentUser();
        return PRETTY_PROFILE;
    }

    @NotNull
    public String viewUserChangePassword() {
        this.user = getCurrentUser();
        return PRETTY_CHANGE_PASSWORD;
    }

    public void generateToken() {
        this.user = userService.generateToken(user);
    }

    private User getCurrentUser() {
        this.user = userService.findLoggedUser();
        return user;
    }

    public boolean hasRole(@NotNull final String checkedRole) {
        @NotNull final Role role = Role.valueOf(checkedRole);
        return getCurrentUser().getRoles().contains(role);
    }

    public void resetRegistryUser() {
        registryUser = new User();
    }

}