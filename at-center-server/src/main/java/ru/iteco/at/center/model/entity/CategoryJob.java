package ru.iteco.at.center.model.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name = "app_categoryjob")
@EqualsAndHashCode(of = "id", callSuper = true)
public class CategoryJob extends AbstractEntity implements Serializable {

    @Nullable
    @ManyToOne
    @JoinColumn(name = "job_id")
    private Job job;

    @Nullable
    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;

}
