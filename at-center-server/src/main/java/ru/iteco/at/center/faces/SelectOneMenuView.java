package ru.iteco.at.center.faces;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.iteco.at.center.model.entity.MetrikValue;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.SelectItem;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
@Setter
@SessionScoped
@ManagedBean(name = "selectOneMenuView")
public class SelectOneMenuView {


    private String selectedOption;

    private void update() {
        System.out.println("HI");
        System.out.println(selectedOption);
    }


//    private String rtl;
//    private String hideNoSelectOption;
//
//    private String countryGroup;
//    private List<SelectItem> countriesGroup;
//
//    private String city;
//    private Map<String, String> cities = new HashMap<>();
//
//    private Country country;
//    private List<Country> countries;
//
//    private String option;
//    private List<String> options;
//
//    private String longItemLabel;
//    private String labeled;

//    @Inject
//    private CountryService service;
//
//    @PostConstruct
//    public void init() {
//
//        countriesGroup = new ArrayList<>();
//
//        SelectItemGroup europeCountries = new SelectItemGroup("Europe Countries");
//        europeCountries.setSelectItems(new SelectItem[]{
//                new SelectItem("Germany", "Germany"),
//                new SelectItem("Turkey", "Turkey"),
//                new SelectItem("Spain", "Spain")
//        });
//
//        SelectItemGroup americaCountries = new SelectItemGroup("America Countries");
//        americaCountries.setSelectItems(new SelectItem[]{
//                new SelectItem("United States", "United States"),
//                new SelectItem("Brazil", "Brazil"),
//                new SelectItem("Mexico", "Mexico")
//        });
//
//        countriesGroup.add(europeCountries);
//        countriesGroup.add(americaCountries);
//
//        //cities
//        cities = new HashMap<>();
//        cities.put("New York", "New York");
//        cities.put("London", "London");
//        cities.put("Paris", "Paris");
//        cities.put("Barcelona", "Barcelona");
//        cities.put("Istanbul", "Istanbul");
//        cities.put("Berlin", "Berlin");
//
//        //countries
//        countries = service.getCountries();
//
//        //options
//        options = new ArrayList<>();
//        for (int i = 0; i < 20; i++) {
//            options.add("Option " + i);
//        }
//    }

}
