package ru.iteco.at.center.exception;

public class EmptyRoleSetException extends RuntimeException {

    public EmptyRoleSetException() {
        super("Error! Role List is Empty!");
    }

}
