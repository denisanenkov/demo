-- Drop table

-- DROP TABLE public.app_category;

CREATE TABLE public.app_category (
	id varchar(255) NOT NULL,
	"name" varchar(255) NULL,
	CONSTRAINT app_category_pkey PRIMARY KEY (id)
);

-- Drop table

-- DROP TABLE public.app_categoryjob;

CREATE TABLE public.app_categoryjob (
	id varchar(255) NOT NULL,
	category_id varchar(255) NULL,
	job_id varchar(255) NULL,
	CONSTRAINT app_categoryjob_pkey PRIMARY KEY (id),
	CONSTRAINT fka4wiqisvqsh3oi10xpxt3ujol FOREIGN KEY (category_id) REFERENCES app_category(id),
	CONSTRAINT fkrf7ig2fhiup5djtxksbfx27mk FOREIGN KEY (job_id) REFERENCES app_job(id)
);

-- Drop table

-- DROP TABLE public.app_job;

CREATE TABLE public.app_job (
	id varchar(255) NOT NULL,
	buildnumber varchar(255) NULL,
	"name" varchar(255) NULL,
	CONSTRAINT app_job_pkey PRIMARY KEY (id)
);

-- Drop table

-- DROP TABLE public.app_jobinstance;

CREATE TABLE public.app_jobinstance (
	id varchar(255) NOT NULL,
	datebegin timestamp NULL,
	dateend timestamp NULL,
	memoryofjob varchar(255) NULL,
	statusjob varchar(255) NULL,
	job_id varchar(255) NULL,
	system_id varchar(255) NULL,
	user_id varchar(255) NULL,
	CONSTRAINT app_jobinstance_pkey PRIMARY KEY (id),
	CONSTRAINT fk8ajdv19kax3mvey91dytp7dc9 FOREIGN KEY (job_id) REFERENCES app_job(id),
	CONSTRAINT fkh57e51fxcuk9kygw924t7ubve FOREIGN KEY (user_id) REFERENCES app_user(id),
	CONSTRAINT fki46r788r9gv8bt8y77jk6kxrd FOREIGN KEY (system_id) REFERENCES app_system(id)
);

-- Drop table

-- DROP TABLE public.app_metrik;

CREATE TABLE public.app_metrik (
	id varchar(255) NOT NULL,
	"name" varchar(255) NULL,
	"type" varchar(255) NULL,
	jobinstance_id varchar(255) NULL,
	CONSTRAINT app_metrik_pkey PRIMARY KEY (id),
	CONSTRAINT fk568ss3vmskb8870r39xly8h3 FOREIGN KEY (jobinstance_id) REFERENCES app_jobinstance(id)
);

-- Drop table

-- DROP TABLE public.app_metrikvalue;

CREATE TABLE public.app_metrikvalue (
	id varchar(255) NOT NULL,
	"timestamp" timestamp NULL,
	value varchar(255) NULL,
	metrik_id varchar(255) NULL,
	CONSTRAINT app_metrikvalue_pkey PRIMARY KEY (id),
	CONSTRAINT fkh39iwg9o034b6epb8e8wihvdp FOREIGN KEY (metrik_id) REFERENCES app_metrik(id)
);

-- Drop table

-- DROP TABLE public.app_system;

CREATE TABLE public.app_system (
	id varchar(255) NOT NULL,
	"name" varchar(255) NULL,
	systemcapacity varchar(255) NULL,
	CONSTRAINT app_system_pkey PRIMARY KEY (id)
);

-- Drop table

-- DROP TABLE public.app_user;

CREATE TABLE public.app_user (
	id varchar(255) NOT NULL,
	email varchar(255) NULL,
	firstname varchar(255) NULL,
	lastname varchar(255) NULL,
	login varchar(255) NULL,
	middlename varchar(255) NULL,
	"password" varchar(255) NULL,
	"role" varchar(255) NULL,
	"token" varchar(255) NULL,
	CONSTRAINT app_user_pkey PRIMARY KEY (id),
	CONSTRAINT uk_irayhia1ygarvmv7apksctnqn UNIQUE (login)
);

-- Drop table

-- DROP TABLE public.app_user_role;

CREATE TABLE public.app_user_role (
	user_id varchar(255) NOT NULL,
	roles varchar(255) NULL,
	CONSTRAINT fkfnlxi1bmv5ao8u3nf30ymq7xa FOREIGN KEY (user_id) REFERENCES app_user(id)
);
