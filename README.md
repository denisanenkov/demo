## AT-CENTER

The automated testing system is designed to perform various kinds and directions of tests. The application allows you to collect statistics on tests, and make a comparative analysis of their performance on different operating systems, on devices with different computing capacities.
A schema of the domain and table relationships in the database, SQL queries for generating tables, and graph descriptions can be found in the [doc](./doc) directory.

### Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

To start the system you need: tomcat server, maven, java 1.8.

### Installing

```
mvn clean install
```

### Running the tests

    @Future junit tests

### Deployment

Add war file in project directory of root tomcat dir.

### Built With

* [Java](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) - Development environment for building applications, applets, and components using the Java programming language.
* [Maven](https://maven.apache.org/) - Dependency Management
* [PrimeFaces](https://www.primefaces.org) - Open source UI framework for JSF featuring over 100 components, touch optimized mobilekit, push framework, client side validation, theme engine and more.

### Versioning

We use [GitLab](https://gitlab.com/) for versioning. 

### Authors

Two previous student graduations

### License

This project is licensed for I-Teco company

### Acknowledgments

[Previous Students](http://sk.volnenko.school/streams/)
