FROM tomcat:9-jdk8
LABEL maintainer="denis@volnenko.ru"

RUN rm -rf /usr/local/tomcat/webapps/*
COPY ./at-center-server/target/at-center.war /usr/local/tomcat/webapps/ROOT.war

EXPOSE 8080

CMD ["catalina.sh", "run"]